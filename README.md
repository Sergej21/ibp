# README #
Copyright (c) 2016, Pavel Juhaňák
-------------------------------------------------------------------------------

## Licence ##
Tento zdrojový kód, jakožto i celý tento nástroj je dostupný pod OPEN SOURCE licencí VUT V BRNĚ.

Úplné znění licence dostupné v souboru `LICENSE.md`.

## Info ##
Tento nástroj je programovou částí bakalářské práce na téma: Generování a ochrana proti DOS útoku na aplikační vrstvě. Tento nástroj umožňuje provádění tří DoS útoků a to HTTP GET útok, HTTP POST útok a DNS amplification útok.

Pro více informací si prosím přečtěte textovou část této práce obsaženou v souboru **`juhanak_BP.pdf`**, nebo dokumentaci vygenerovanou nástrojem Doxygen obsaženou v souboru **`manual.pdf`**.

## Překlad ##
Pro standartní běh nástroje překládáme pomocí příkazu `make`. Pokročilejšího chování dosáhneme pomocí příkazu `make` společně s jedním z parametrů:

* `debug` - pro detailnější debugovací výpisy

* `rebuild` - pro smazání a znovupřeložení projektu

* `run` - pro běh s ukázkovými parametry

* `clean` - pro smazání souborů vzniklých při překladu

## Spuštění ##
Nástroj je v současné době plně funkční pouze pod operačním systémem Linux.

Pro standartní běh je nástroji předáván jeden parametr a to XML soubor obsahující požadované parametry spuštění, validní podle DTD souboru `dos.dtd`. Ukázku validního XML souboru s parametry můžeme nalézt v souboru `params.xml`, nebo v kterémkoliv ze souborů v adresáři `params`.

Pro výpis nápovědy nástroj spustíme s přepínačem `-h`, nebo `--help`.

Příklady spuštění:

* `dos_tool params.xml`

* `dos_tool --help`

## Ukončení ##
Legitimní ukončení probíhá zasláním některého ze signálů:

* `SIGHUP`

* `SIGTERM`

* `SIGTSTP`

Signál `SIGINT` není odchytáván, aby bylo možné nástroj "násilně" ukončit.

## Návratové hodnoty ##
| Hodnota | Situace       |
| ------- | ------------- |
| 0       | úspěch        |
| 1       | chyba         |
| 2       | tisk nápovědy |

## Závislosti ##
Pro správnou funkčnost nástroje je zapotřebí nainstalovat knihovnu `libxml2`, která je distribuována pod MIT licencí

* knihovna dostupná z: [http://www.xmlsoft.org/index.html](http://www.xmlsoft.org/index.html)

* úplné znění licence [https://opensource.org/licenses/mit-license.html](https://opensource.org/licenses/mit-license.html)

### Linux ###
Knihovnu můžete nainstalovat podle návodu na stránkách knihovny, nebo zadáním následujících dvou příkazů, pokud používáte OS Ubuntu

```
sudo apt-get install libxml2-dev
```

```
sudo apt-get install libboost-all-dev
```

### Windows ###
Binární soubory knihovny a jejích závislostí můžete získat ze stránek knihovny, nebo můžete použít binární soubory obsažené v adresáři `libs`, které jsou stažené právě z těchto stránek.

## Převzatý kód ##
### Generátor pseudonáhodných čísel ###
Generátor pseudonáhodných čísel deklarovaný v souboru `RandomGenerator.hpp`, jehož definice je obsažena v souboru `RandomGenerator.cpp` byl převzat ze společného projektu do předmětu *Modelování a simulace (IMS)*, vytvořeného v roce 2015. Autory tohoto projektu jsou **Jan Herec** a **Pavel Juhaňák** (já).

### Funkce pro kontrolní součet ###
Funkce pro počítání kontrolního součtu `checksum()`, která je deklarována v souboru `DoSlib.hpp` a jejíž definice je obsažena v souboru `main.cpp` byla převzata z webové stránky *BinaryTides*, konkrétně tutoriálu *Programming raw udp sockets in C on Linux*, dostupného z: [http://www.binarytides.com/raw-udp-sockets-c-linux/](http://www.binarytides.com/raw-udp-sockets-c-linux/). Tento tutoriál byl zveřejněn v roce 2012 a jeho autorem je uživatel pod pseudonymem **Silver Moon**.

## ToDo ##
Implementace kompatibility s operačním systémem Windows.
