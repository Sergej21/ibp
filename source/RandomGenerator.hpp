///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    RandomGenerator.hpp                                        ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojovy kod, jakozto i cely tento nastroj           ///
///                je dostupny pod OPEN SOURCE licenci VUT V BRNE           ///
///              Uplne zneni licence dostupne v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#ifndef RANDOMGENERATOR_HPP_INCLUDED
#define RANDOMGENERATOR_HPP_INCLUDED


// konstanty pro praci generatoru pseudonahodnych cisel (trida RandomGenerator)
// pozivame konstanty glibc (pouzite v GCC)
#define DEFAULT_MULTIPLIER        1103515245  // defaultni nasobitel nahodneho cisla
#define DEFAULT_INCREMENT         12345       // defaultni prirustek nahodneho cisla
// modulo/RAND_MAX je nastaveno na UINT_MAX (2^32)
// seminko je defaultne spocitano z funkce casu


/**
 * Trida generatoru pseudonahodnych cisel
 * - pracuje na principu linearniho kongruentniho generatoru
 */
class RandomGenerator {
    private:
        unsigned int random_number;  // aktualni nahodne cislo
        unsigned int multiplier;     // nasobitel nahodneho cisla
        unsigned int increment;      // prirustek nahodneho cisla

        /**
         * Spocita dalsi pseudonahodne cislo
         * - z intervalu  <0, RAND_MAX)
         * - pracuje na principu linearniho kongruentniho generatoru
         * @return  double  Pseudonahodne cislo z intervalu <0, RAND_MAX)
         */
        unsigned int getNext ();

    public:
        /**
         * Konstruktor tridy RandomGenerator
         * - pro zajisteni nahodnosti se seminko defaultne spocita z funkce casu
         * @param  multiplier  Nasobitel nahodneho cisla
         * @param  increment   Prirustek nahodneho cisla
         */
        RandomGenerator (unsigned int multiplier = DEFAULT_MULTIPLIER, unsigned int increment = DEFAULT_INCREMENT);

        /**
         * Metoda pro nastaveni seminka
         * @param  seed  Seminko (poratecni hodnota generovanych cisel)
         */
        void setSeed (unsigned int seed);

        /**
         * Generuje normalizovana pseudonahodna cisla rovnomerneho rozlozeni
         * - v intervalu <0, 1)
         * @return  double  Pseudonahodne cislo z intervalu <0, 1)
         */
        double normalized ();

        /**
         * Generuje pseudonahodna cisla rovnomerneho rozlozeni
         * - ve zvolenem intervalu <min_val, max_val)
         * @param   min_val  Dolni mez intervalu
         * @param   max_val  Horni mez intervalu
         * @return  double   Pseudonahodne cislo z intervalu <min_val, max_val)
         */
        double uniform (double min_val, double max_val);

        /**
         * Generuje pseudonahodna cisla exponencialniho rozlozeni
         * - pouziva metodu inverzni transformace
         * @param   mean    Stredni hodnota
         * @return  double  Pseudonahodne cislo
         */
        double exponencial (double mean);

        /**
         * Generuje pseudonahodna cisla gaussova (normalniho) rozlozeni
         * - funguje na tom principu, ze pokud secteme 12 pseudonahodnych cisel
         *   z intervalu <0, 1) a od souctu odecteme cislo 6, tak dostaneme pseudonahodne
         *   cislo gaussova rozlozeni v intervalu <0, 1)
         * - pote toto cislo prepocitame do pozadovaneho "intervalu"
         * @param   mean       Stredni hodnota
         * @param   deviation  Smerodatna odchylka
         * @return  double     Pseudonahodne cislo
         */
        double gaussian (double mean, double deviation);
};

#endif // RANDOMGENERATOR_HPP_INCLUDED
