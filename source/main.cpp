///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    main.cpp                                                   ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////
/**
 * Filtry pro wireshark
 *
 * tcp.port == 8080
 * dns && (ip.dst == 192.168.1.25 || ip.src == 192.168.1.25)
 */

/**
 * Vytvoreni smart pointeru na novou instanci objektu,
 * vytvoreni wrapperu metody obektu konkretni instance,
 * invokace teto metody
 *
    auto anInstance = std::make_shared<A>();
    std::function<int(double)> fnCaller = std::bind(&A::fn, anInstance, std::placeholders::_1);
    fnCaller();
 */

#include "DoSlib.hpp"

using namespace std;


// seznamy startovacich a ukoncovacich metod utoku
vector<std::function<void()>> start_vec;
vector<std::function<void()>> end_vec;

#ifdef LINUX

pthread_t thread[3]; // identifikatory vlaken, 3 protoze muzou byt spusteny max 3 utoky naraz
sem_t semaphore;     // semafor pro synchronizaci vytvareni vlaken

#else
#endif

// instance generatoru pseudonahodnych cisel
std::shared_ptr<RandomGenerator>  random_generator = std::make_shared<RandomGenerator>();


/**
 * Spocita kontrolni soucet
 *  - funkce s drobnymi upravami prevzata z:
 *    http://www.binarytides.com/raw-udp-sockets-c-linux/
 * @param  *struct_ptr      Ukazatel na strukturu, jejiz kontrolni soucet pocitam
 * @param   len             Delka struktury
 * @return  unsigned short  Kontrolni soucet
 */
unsigned short checksum(unsigned short *struct_ptr, int len) {
    unsigned int sum = 0;
    unsigned short *struct_ptr_tmp = (unsigned short *)struct_ptr;

    while (len > 1) {
        sum += *struct_ptr_tmp++;
        len -= 2;
    }

    if (len == 1) {
        sum += (unsigned short) *((unsigned char *)struct_ptr_tmp);
    }

    sum  = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);

    return (unsigned short)(~sum);
}


/**
 * Orizne bile znaky
 * - z predaneho retezce orizne pocatecni a koncove bile znaky
 * @param  &str  Retezec pro orezani
 */
void trim (string& str) {
    size_t pos = str.find_first_not_of(" \t\n\v\f\r");
    str.erase(0, pos);

    pos = str.find_last_not_of(" \t\n\v\f\r");
    if (string::npos != pos)
        str.erase(pos+1);
}


/**
 * Vrati nahodny retezec o zvolene velikosti
 * @param   len     Pozadovana velikost
 * @return  string  Nahodny retezec
 */
string getRandStr (int len) {
    ostringstream oss;
    int rand_num;

    for (int i = 0; i < len; ++i) {
        rand_num = random_generator->uniform(ASC_MIN, ASC_MAX);
        if (rand_num == 64) {
            rand_num = 61; // znak =
        }
        else if (rand_num > 90) {
            rand_num += 6; // mala pismena bez specialnich zanku
        }

        oss << (char)rand_num;
    }

    return oss.str();
}


/**
 * Vrati aktualni cas v pozadovanem formatu
 * @return  string  Retezec s casem
 */
string getTime () {
    ostringstream oss;
    time_t rawtime;
    struct timeval tv_now;
    char buf[25];

    gettimeofday(&tv_now, NULL); // aktualni cas

    rawtime = tv_now.tv_sec;
    strftime(buf, 25, "%Y-%m-%d %H:%M:%S.", localtime(&rawtime));

    oss << buf;
    oss << setfill('0') << setw(3) << (tv_now.tv_usec)/1000;

    return oss.str();
}


/**
 * Vrati predany cas v pozadovanem formatu
 * @param   tv_time  Cas pro prevod na retezec
 * @return  string   Retezec s casem
 */
string getTime (struct timeval tv_time) {
    ostringstream oss;
    time_t rawtime;
    char buf[25];

    rawtime = tv_time.tv_sec;
    strftime(buf, 25, "%Y-%m-%d %H:%M:%S.", localtime(&rawtime));

    oss << buf;
    oss << setfill('0') << setw(3) << (tv_time.tv_usec)/1000;

    return oss.str();
}


/**
 * Obalovaci funkce pro startovaci metody utoku
 * - kvuli pousteni utoku ve vlaknech
 * - funkce invokuje startovaci metodu utoku
 * - po skonceni utoku zavre vlakno
 * @param  *index_ptr  Index do pole startovacich metod utoku
 */
void *startMethodWrapper(void *index_ptr) {
    int index = *(int *)index_ptr;
    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> START startMethodWrapper(%d)\n", index);
#ifdef LINUX
    // dam hlavnimu vlaknu vedet, ze se vlakno vytvorilo
    sem_post(&semaphore);

    start_vec[index]();

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> pthread_exit(%d)\n", index);
    pthread_exit(NULL);
#else
#endif
}


/**
 * Signal Handler
 * @param  signum  Cislo odchyceneho signalu
 */
void signalHandler (int signum) {
    bool sig_usr = false;

#ifdef LINUX
    if (signum == SIGUSR1){
        sig_usr = true;
        if (DEBUG_MSG == true) fprintf(stderr, "\n\nINFO -> caught signal SIGUSR1\n");
    }
#else
#endif

    if (!sig_usr) {
#ifdef LINUX
        switch (signum) {
            case SIGHUP:
                if (DEBUG_MSG == true) fprintf(stderr, "\n\nINFO -> caught signal SIGHUP\n");
                break;

            case SIGINT:
                if (DEBUG_MSG == true) fprintf(stderr, "\n\nINFO -> caught signal SIGINT\n");
                break;

            case SIGTERM:
                if (DEBUG_MSG == true) fprintf(stderr, "\n\nINFO -> caught signal SIGTERM\n");
                break;

            case SIGTSTP:
                if (DEBUG_MSG == true) fprintf(stderr, "\n\nINFO -> caught signal SIGTSTP\n");
                break;
        }
#else
#endif

        // iterator pres ukoncovaci metody
        auto it = begin(end_vec);

        // projde ukoncovaci metody ulozene v seznamu a invokuje je
        for (; it != end(end_vec); ++it) {
            (*it)(); // invokace ukoncovaci metody
        }

#ifdef LINUX
        // posle signal SIGUSR1 do vsech vlaken
        int thread_count = start_vec.size();
        for (int i = 0; i < thread_count; ++i) {
            pthread_kill(thread[i], SIGUSR1);
        }
#else
#endif

        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> END of signalHandler()\n\n");
    }
}


/**
 * Hlavni program
 * @param   argc    Pocet parametru
 * @param  *argv[]  Parametry prikazove radky
 * @return  0       Pri uspechu
 * @return  1       Pri chybe
 * @return  2       Pri tisku napovedy
 */
int main (int argc, char const *argv[]) {
#ifdef LINUX
    // odchytavani signalu
    signal(SIGHUP,  signalHandler);  // Hangup             [01]
    // SIGINT neodchytavam, aby se dal program natvrdo ukoncit
    //signal(SIGINT,  signalHandler);  // Interrupt (ctrl-c) [02]
    signal(SIGTERM, signalHandler);  // Termination        [15]
    signal(SIGUSR1, signalHandler);  // User signal 1      [16]
    signal(SIGTSTP, signalHandler);  // Stop (ctrl-z)      [20]
    signal(SIGPIPE, SIG_IGN);        // Ignoruju SIGPIPE, aby mohl byt zpracovan jako error
#else
#endif

    std::function<void()> method_wrapper;
    start_vec.clear();
    end_vec.clear();

    int ret_val;
    // zparsuje a nastavi parametry
    ret_val = ParameterParser::get(argc, argv);
    if (ret_val == ERROR) {
        return ERROR;
    }
    else if (ret_val == HELP) {
        return HELP;
    }

    // pripravi GET utok na spusteni
    if (GETattack::isActive()) {
        method_wrapper = std::bind(&GETattack::start);
        start_vec.push_back(method_wrapper);

        method_wrapper = std::bind(&GETattack::end);
        end_vec.push_back(method_wrapper);
    }

    // pripravi POST utok na spusteni
    if (POSTattack::isActive()) {
        method_wrapper = std::bind(&POSTattack::start);
        start_vec.push_back(method_wrapper);

        method_wrapper = std::bind(&POSTattack::end);
        end_vec.push_back(method_wrapper);
    }

    // pripravi DNS utok na spusteni
    if (DNSattack::isActive()) {
#ifdef LINUX
        int root = getuid();
#else
        int root = 0;
#endif
        if (root == 0) {
            method_wrapper = std::bind(&DNSattack::start);
            start_vec.push_back(method_wrapper);

            method_wrapper = std::bind(&DNSattack::end);
            end_vec.push_back(method_wrapper);
        } else {
            fprintf(stderr, "WARNING -> run as ROOT/ADMIN to perform DNS attack\n\n");
        }
    }

    method_wrapper = NULL;

#ifdef LINUX
    // inicializuje semafor, bude sdileny mezi vlakny a jeho pocatecni hodnota bude 0
    sem_init(&semaphore, 1, 0);

    // pro kazdy nastaveny utok otevre vlakno a spusti ho v nem
    int thread_count = start_vec.size();
    for (int i = 0; i < thread_count; ++i) {
        if (pthread_create(&thread[i], NULL, &startMethodWrapper, (void *)&i) != 0) {
            fprintf(stderr, "ERROR -> pthread_create()\n\n");
        } else {
            if (DEBUG_MSG == true) fprintf(stderr, "INFO -> pthread_create(%d)\n", i);
        }

        // pocka, az se vlakno skutecne vytvori
        sem_wait(&semaphore);
    }

    // smaze semafor, uz neni potreba
    sem_destroy(&semaphore);

    // ceka na ukonceni vsech vlaken
    for (int i = 0; i < thread_count; ++i) {
        if (pthread_join(thread[i], NULL) != 0) {
            fprintf(stderr, "ERROR -> pthread_join()\n\n");
        }

        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> pthread_join(%d)\n", i);
    }
#else
#endif

    // vymaze startovaci a ukoncovaci seznam
    start_vec.clear();
    end_vec.clear();

    return SUCCESS;

    /**
     * Puvodni verze spousteni
     * - pred pouzitim vlaken
     */
    /*
    // iterator pres startovaci metody
    auto it = begin(start_vec);

    // projde startovaci metody ulozene v seznamu, pro kazdou otevre nove vlakno a invokuje ji v nem
    for (; it != end(start_vec); ++it) {
        // TODO otevre nove vlakno a v nem invokuje startovaci statickou metodu

        (*it)(); // invokace startovaci metody
    }
    */
}
