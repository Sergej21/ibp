///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    ParameterParser.hpp                                        ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////
/**
 * Pro parsovani parametru pouzita knihovna libxml2 dostupna z:
 * http://www.xmlsoft.org/index.html
 * tato knihovna je distribuovana pod MIT licenci, uplne zneni licence dostupne z:
 * https://opensource.org/licenses/mit-license.html
 */

#ifndef PARAMETERPARSER_HPP_INCLUDED
#define PARAMETERPARSER_HPP_INCLUDED

#include "DoSlib.hpp"

#include <libxml/parser.h>
#include <libxml/tree.h>

using namespace std;

/**
 * Staticka trida parseru parametru
 */
class ParameterParser {
    public:
        /**
         * Staticka metoda zjisti, zda je podporovano parsovani XML
         * - pokud ano, invokuje metodu "ParameterParser::parse(argc, argv)"
         * - pokud ne,  konci s chybou
         * @param   argc     Pocet parametru prikazove radky
         * @param  *argv     Parametry prikazove radky
         * @return  SUCCESS  Pri uspechu
         * @return  ERROR    Pri chybe
         */
        static int get (int argc, char const *argv[]);

    private:
        /**
         * Staticka metoda pro parsovani parametru a jejich ulozeni do prislusnych trid
         * - parametry jsou cteny z XML souboru predaneho parametrem prikazove radky
         * @param   argc     Pocet parametru prikazove radky
         * @param  *argv     Parametry prikazove radky
         * @return  SUCCESS  Pri uspechu
         * @return  ERROR    Pri chybe
         * @return  HELP     Pri tisku napovedy
         */
        static int parse (int argc, char const *argv[]);

        /**
         * Staticka metoda pro parsovani parametru GET utoku
         * - prochazi XML stromem GET utoku a parametry nastavuje do
         *   staticke tridy GET utoku
         * @param   doc       Strom XML dokumentu
         * @param   cur_node  Uzel, na kterem se ve stromu nachazim
         * @return  SUCCESS   Pri uspechu
         * @return  ERROR     Pri chybe
         */
        static int parseGET (xmlDocPtr doc, xmlNodePtr cur_node);

        /**
         * Staticka metoda pro parsovani parametru POST utoku
         * - prochazi XML stromem POST utoku a parametry nastavuje do
         *   staticke tridy POST utoku
         * @param   doc       Strom XML dokumentu
         * @param   cur_node  Uzel, na kterem se ve stromu nachazim
         * @return  SUCCESS   Pri uspechu
         * @return  ERROR     Pri chybe
         */
        static int parsePOST (xmlDocPtr doc, xmlNodePtr cur_node);

        /**
         * Staticka metoda pro parsovani parametru DNS utoku
         * - prochazi XML stromem DNS utoku a parametry nastavuje do
         *   staticke tridy DNS utoku
         * @param   doc       Strom XML dokumentu
         * @param   cur_node  Uzel, na kterem se ve stromu nachazim
         * @return  SUCCESS   Pri uspechu
         * @return  ERROR     Pri chybe
         */
        static int parseDNS (xmlDocPtr doc, xmlNodePtr cur_node);

        /**
         * Staticka metoda zjisti o jakou distribucni funkci se jedna a vrati jeji kod a parametry
         * - parametry vraci pomoci promennych predanych odkazem
         * - pokud rozlozeni nektery z parametru nepouziva, jeho hodnota se nemeni
         * - pokud je zadano konstantni rozlozeni, jeho hodnota se vraci v promenne "mean"
         * - pokud je zadane minimum vetsi nez maximum, jejich hodnoty se prohodi
         * @param   doc             Strom XML dokumentu
         * @param   cur_node        Uzel, na kterem se ve stromu nachazim
         * @param  *mean            Ukazatel na stredni hodnotu
         * @param  *dev             Ukazatel na smerodatnou odchylku
         * @param  *min             Ukazatel na minimum
         * @param  *max             Ukazatel na maximum
         * @return  unsigned short  Kod rozlozeni - definovane v DoSlib.hpp
         */
        static unsigned short parseDistr (xmlDocPtr doc, xmlNodePtr cur_node, unsigned int* mean, unsigned int* dev, unsigned int* min, unsigned int* max);
};

#endif // PARAMETERPARSER_HPP_INCLUDED
