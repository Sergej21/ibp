///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    CalendarOfEvents.hpp                                       ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#ifndef CALENDAROFEVENT_HPP_INCLUDED
#define CALENDAROFEVENT_HPP_INCLUDED

#include "DoSlib.hpp"

using namespace std;

/**
 * Trida aktivacniho zaznamu udalosti, ktery ukladame do kalendare udalosti
 */
class ActivationRecord {
    public:
        std::function<void()> event;  // wrapper, ktery invokuje metodu, do nej zabalenou (pozadovanou udalost)
        struct timeval tv_act_time;   // struktura s presnym aktivacnim casem udalosti
};

/**
 * Trida kalendare udalosti
 */
class CalendarOfEvents {
    public:
        /**
         * Metoda pro vlozeni aktivacniho zaznamu do kalendare
         * - kalendar je vzdy serazeny podle aktivacniho casu
         * - pri vkladani hleda a vklada na spravne misto
         * - vklada pouze pokud je kalendar aktivni
         * @param  act_rec  Objekt ActivationRecord obsahujici aktivacni zaznam
         */
        void add (std::shared_ptr<ActivationRecord> act_rec);

        /**
         * Metoda vrati prvni aktivacni zaznam a odstrani ho z kalendare
         * @return  std::shared_ptr<ActivationRecord>  Aktivacni zaznam udalosti
         * @return  NULL                               Pokud byl kalendar prazdny
         */
        std::shared_ptr<ActivationRecord> popFront ();

        /**
         * Metoda pro odstraneni vsech aktivacnich zaznamu z kalendare
         */
        void clear ();

        /**
         * Metoda vraci zda je kalendar prazdny
         * @return  1  Pokud je prazdny
         * @return  0  Pokud neni prazdny
         */
        int empty ();

        /**
         * Metoda deaktivuje kalendar udalosti
         * - nastavi "active" na false
         * - odstrani zaznamy z kalendare
         */
        void deactivate ();

    private:
        // datova struktura uchovavajici seznam aktivacnich zaznamu udalosti
        vector<std::shared_ptr<ActivationRecord>> activation_records;
        bool active = true;  // znaci zda je kalendar aktivni
};

#endif // CALENDAROFEVENT_HPP_INCLUDED
