///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    ParameterParser.cpp                                        ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////
/**
 * Pro parsovani parametru pouzita knihovna libxml2 dostupna z:
 * http://www.xmlsoft.org/index.html
 * tato knihovna je distribuovana pod MIT licenci, uplne zneni licence dostupne z:
 * https://opensource.org/licenses/mit-license.html
 */

#include "DoSlib.hpp"

using namespace std;


/**
 * Staticka metoda zjisti, zda je podporovano parsovani XML
 * - pokud ano, invokuje metodu "ParameterParser::parse(argc, argv)"
 * - pokud ne,  konci s chybou
 * @param   argc     Pocet parametru prikazove radky
 * @param  *argv     Parametry prikazove radky
 * @return  SUCCESS  Pri uspechu
 * @return  ERROR    Pri chybe
 */
int ParameterParser::get (int argc, char const *argv[]) {
    #ifdef LIBXML_TREE_ENABLED
        return ParameterParser::parse(argc, argv);
    #else
        fprintf(stderr, "ERROR -> Libxml2 tree not supported\n\n");
        return ERROR;
    #endif
}


/**
 * Staticka metoda pro parsovani parametru a jejich ulozeni do prislusnych trid
 * - parametry jsou cteny z XML souboru predaneho parametrem prikazove radky
 * @param   argc     Pocet parametru prikazove radky
 * @param  *argv     Parametry prikazove radky
 * @return  SUCCESS  Pri uspechu
 * @return  ERROR    Pri chybe
 */
int ParameterParser::parse (int argc, char const *argv[]) {
    xmlParserCtxtPtr ctxt;       // kontext XML parseru
    xmlDocPtr  doc;              // strom XML dokumentu
    xmlNodePtr root_element;     // korenovy uzel
    xmlNodePtr cur_node;         // aktualni uzel
    xmlNodePtr tmp_node;         // pomocny  uzel

    int error_flag = 0;          // flag chyby
    int get_error  = 0;          // flag chyby z parsovani GET utoku
    int get_flag   = 0;          // flag GET  utoku
    int post_flag  = 0;          // flag POST utoku
    int post_error = 0;          // flag chyby z parsovani POST utoku
    int dns_flag   = 0;          // flag DNS  utoku
    int dns_error  = 0;          // flag chyby z parsovani DNS utoku

    if (argc > 2) {
        fprintf(stderr, "ERROR -> Wrong number of parameters\n\n");
        return ERROR;
    }
    else if (argc < 2) {
        fprintf(stderr, "ERROR -> Missing parameter\n\n");
        return ERROR;
    }

    if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        cout << endl << " # README #" << endl
        << " Copyright (c) 2016, Pavel Juhanak" << endl
        << " -------------------------------------------------------------------------------" << endl << endl
        << " ## Licence ##" << endl
        << " Tento zdrojovy kod, jakozto i cely tento nastroj je dostupny pod" << endl
        << "   OPEN SOURCE licenci VUT V BRNE." << endl
        << " Uplne zneni licence dostupne v souboru `LICENSE.md`." << endl << endl
        << " ## Info ##" << endl
        << " Tento nastroj je programovou casti bakalarske prace na tema: Generovani" << endl
        << "   a ochrana proti DOS utoku na aplikacni vrstve. Tento nastroj umoznuje" << endl
        << "   provadeni tri DoS utoku a to HTTP GET utok, HTTP POST utok" << endl
        << "   a DNS amplification utok." << endl
        << " Pro vice informaci si prosim prectete textovou cast teto prace obsazenou" << endl
        << "   v souboru **`juhanak_BP.pdf`**, nebo dokumentaci vygenerovanou nastrojem" << endl
        << "   Doxygen obsazenou v souboru **`manual.pdf`**." << endl << endl
        << " ## Preklad ##" << endl
        << " Pro standartni beh nastroje prekladame pomoci prikazu `make`." << endl
        << " Pokrocilejsiho chovani dosahneme pomoci prikazu `make`" << endl
        << "   spolecne s jednim z parametru:" << endl << endl
        << "   * `debug`   - pro detailnejsi debugovaci vypisy" << endl
        << "   * `rebuild` - pro smazani a znovupreloreni projektu" << endl
        << "   * `run`     - pro beh s ukazkovymi parametry" << endl
        << "   * `clean`   - pro smazani souboru vzniklych pri prekladu" << endl << endl
        << " ## Spusteni ##" << endl
        << " Pro standartni beh je nastroji predavan jeden parametr a to XML soubor" << endl
        << "   obsahujici pozadovane parametry spusteni, validni podle DTD souboru" << endl
        << "   `dos.dtd`. Ukazku validniho XML souboru s parametry muzeme nalezt v souboru" << endl
        << "   `params.xml`, nebo v kteremkoliv ze souboru v adresari `params`." << endl
        << " Pro vypis napovedy nastroj spustime s prepinacem `-h`, nebo `--help`." << endl
        << " Priklady spusteni:" << endl
        << "   dos_tool params.xml" << endl
        << "   dos_tool --help" << endl << endl
        << " ## Ukoncení ##" << endl
        << " Legitimni ukonceni probiha zaslanim nektereho ze signalu:" << endl
        << "   * `SIGHUP`" << endl
        << "   * `SIGTERM`" << endl
        << "   * `SIGTSTP`" << endl
        << " Signal `SIGINT` neni odchytavan, aby bylo mozne nastroj \"nasilne\" ukoncit." << endl << endl
        << " ## Navratove hodnoty ##" << endl
        << " +---------+---------------+" << endl
        << " | Hodnota | Situace       |" << endl
        << " | ------- | ------------- |" << endl
        << " | 0       | uspech        |" << endl
        << " | 1       | chyba         |" << endl
        << " | 2       | tisk napovedy |" << endl
        << " +---------+---------------+" << endl << endl
        << " ## Zavislosti ##" << endl
        << " Pro spravnou funkcnost nastroje je zapotrebi nainstalovat knihovnu `libxml2`," << endl
        << "   ktera je distribuovana pod MIT licenci." << endl << endl
        << "   * knihovna dostupna z: http://www.xmlsoft.org/index.html" << endl
        << "   * uplne zneni licence https://opensource.org/licenses/mit-license.html" << endl << endl
        << "   ### Linux ###" << endl
        << "   Knihovnu muzete nainstalovat podle navodu na strankach knihovny, nebo zadanim" << endl
        << "     nasledujicich dvou prikazu, pokud pouzivate OS Ubuntu" << endl << endl
        << "     sudo apt-get install libxml2-dev" << endl
        << "     sudo apt-get install libboost-all-dev" << endl << endl
        << "   ### Windows ###" << endl
        << "   Binarni soubory knihovny a jejich zavislosti muzete ziskat ze stranek" << endl
        << "     knihovny, nebo muzete pouzit binarni soubory obsazene v adresari `libs`," << endl
        << "     ktere jsou stazene prave z techto stranek." << endl << endl
        << " ## Prevzaty kod ##" << endl
        << "   ### Generator pseudonahodnych cisel ###" << endl
        << "   Generator pseudonahodnych cisel deklarovany v souboru `RandomGenerator.hpp`," << endl
        << "     jehoz definice je obsazena v souboru `RandomGenerator.cpp` byl prevzat" << endl
        << "     ze spolecneho projektu do predmetu *Modelovani a simulace (IMS)*," << endl
        << "     vytvoreneho v roce 2015. Autory tohoto projektu jsou" << endl
        << "     **Jan Herec** a **Pavel Juhanak** (ja)." << endl << endl
        << "   ### Funkce pro kontrolni soucet ###" << endl
        << "   Funkce pro pocitani kontrolniho souctu `checksum()`, ktera je deklarovana" << endl
        << "     v souboru `DoSlib.hpp` a jejiz definice je obsazena v souboru `main.cpp`" << endl
        << "     byla prevzata z webove stranky *BinaryTides*, konkretne tutorialu" << endl
        << "     *Programming raw udp sockets in C on Linux*, dostupneho" << endl
        << "     z: http://www.binarytides.com/raw-udp-sockets-c-linux/." << endl
        << "     Tento tutorial byl zverejnen v roce 2012 a jeho autorem je uzivatel" << endl
        << "     pod pseudonymem **Silver Moon**." << endl << endl
        << " ## ToDo ##" << endl
        << " Implementace kompatibility s operacnim systemem Windows." << endl << endl;

        return HELP;
    }

    LIBXML_TEST_VERSION          // inicializuje a zkontroluje Libxml2 knihovnu

    ctxt = xmlNewParserCtxt();   // vytvori kontext parseru
    if (ctxt == NULL) {
        fprintf(stderr, "ERROR -> Failed to allocate parser context\n\n");
        return ERROR;
    }

    /* parsuje soubor, provadi DTD validaci, neuvazuje prazdne uzly */
    doc = xmlCtxtReadFile(ctxt, argv[1], NULL, XML_PARSE_DTDVALID + XML_PARSE_NOBLANKS);
    if (doc == NULL) {           // chyba v parsovani (spatny soubor atd.)
        fprintf(stderr, "ERROR -> Failed to parse %s\n\n", argv[1]);
        error_flag = 1;
    } else {
        if (ctxt->valid == 0) {  // soubor neni validni podle DTD
            fprintf(stderr, "ERROR -> Failed to validate %s\n\n", argv[1]);
            error_flag = 1;
        }
    }

    if (!error_flag) {           // nedoslo k chybe
        root_element = xmlDocGetRootElement(doc); // ziska korenovy uzel

        /* projde vsechny uzly "attack" uroven (attack+) */
        for (cur_node = root_element->xmlChildrenNode; cur_node; cur_node = cur_node->next) {
            tmp_node = cur_node->xmlChildrenNode; // tmp_node = (get|post|dns)

            /* GET utok */
            if (!xmlStrcmp(tmp_node->name, (const xmlChar *)"get")) {
                if (get_flag) {  // GET utok byl jiz nastaven
                    if (DEBUG_MSG == true) fprintf(stderr, "WARNING -> GET attack redefinition\n\n");
                } else {
                    get_flag = 1;
                }

                get_error = ParameterParser::parseGET(doc, tmp_node->xmlChildrenNode);
            }

            /* POST utok */
            if (!xmlStrcmp(tmp_node->name, (const xmlChar *)"post")) {
                if (post_flag) { // POST utok byl jiz nastaven
                    if (DEBUG_MSG == true) fprintf(stderr, "WARNING -> POST attack redefinition\n\n");
                } else {
                    post_flag = 1;
                }

                post_error = ParameterParser::parsePOST(doc, tmp_node->xmlChildrenNode);
            }

            /* DNS utok */
            if (!xmlStrcmp(tmp_node->name, (const xmlChar *)"dns")) {
                if (dns_flag) { // DNS utok byl jiz nastaven
                    if (DEBUG_MSG == true) fprintf(stderr, "WARNING -> DND attack redefinition\n\n");
                } else {
                    dns_flag = 1;
                }

                dns_error = ParameterParser::parseDNS(doc, tmp_node->xmlChildrenNode);
            }
        }
    }

    xmlFreeParserCtxt(ctxt);    // uvolni kontext parseru
    xmlFreeDoc(doc);            // uvolni dokument
    xmlCleanupParser();         // vycisti co je potreba

    if (error_flag)
        return ERROR;

    if (get_error && post_error && dns_error)
        return ERROR;

    return SUCCESS;
}


/**
 * Staticka metoda pro parsovani parametru GET utoku
 * - prochazi XML stromem GET utoku a parametry nastavuje do
 *   staticke tridy GET utoku
 * @param   doc       Strom XML dokumentu
 * @param   cur_node  Uzel, na kterem se ve stromu nachazim
 * @return  SUCCESS   Pri uspechu
 * @return  ERROR     Pri chybe
 */
int ParameterParser::parseGET (xmlDocPtr doc, xmlNodePtr cur_node) {
    xmlNodePtr tmp_node;
    ostringstream oss;
    string str;
    unsigned short rozlozeni;

    unsigned int mean = 0, dev = 0, min = 0, max = RND_MAX;

                                               // cur_node = victim
    tmp_node = cur_node->xmlChildrenNode;      // tmp_node = ip

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, tmp_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

#ifdef LINUX
    struct hostent *hostent;
    struct in_addr *in;

    if ((hostent = gethostbyname(str.c_str())) == NULL) {
        fprintf(stderr, "ERROR -> Wrong victim IP or name\n\n");
        return ERROR;
    }

    in = (struct in_addr *)hostent->h_addr;

    oss.str("");
    oss.clear();
    oss << inet_ntoa(*in);
#else
#endif
    GETattack::setVictimIP(oss.str());         // ulozim IP

    tmp_node = tmp_node->next;                 // tmp_node = port

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, tmp_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

#ifdef LINUX
    struct servent *servent;
    if ((servent = getservbyport(htons(strtol(str.c_str(), NULL, 0)), NULL)) == NULL) {
        fprintf(stderr, "ERROR -> Wrong victim port\n\n");
        return ERROR;
    }
#else
#endif
    GETattack::setVictimPORT( htons(strtol(str.c_str(), NULL, 0)) ); // ulozim port

    cur_node = cur_node->next;                 // cur_node = num_connections

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

    GETattack::setNumConnections(strtol(str.c_str(), NULL, 0)); // ulozim pocet pripojeni

    cur_node = cur_node->next;                 // cur_node = packet_delay
    tmp_node = cur_node->xmlChildrenNode;      // tmp_node = (const|uniform|exponencial|gaussian)

    /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
    rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
    GETattack::setTimeDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni casu a jeho parametry

    cur_node = cur_node->next;                 // cur_node = start_delay?, NULL

    if (cur_node) {                            // cur_node = start_delay
        mean = 0; dev = 0; min = 0; max = RND_MAX;
        /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
        rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
        GETattack::setStartTimeDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni odlozeneho startu a jeho parametry
    }

    if (DEBUG_MSG == true) GETattack::print(); // vypise ulozene parametry

    return SUCCESS;
}


/**
 * Staticka metoda pro parsovani parametru POST utoku
 * - prochazi XML stromem POST utoku a parametry nastavuje do
 *   staticke tridy POST utoku
 * @param   doc       Strom XML dokumentu
 * @param   cur_node  Uzel, na kterem se ve stromu nachazim
 * @return  SUCCESS   Pri uspechu
 * @return  ERROR     Pri chybe
 */
int ParameterParser::parsePOST (xmlDocPtr doc, xmlNodePtr cur_node) {
    xmlNodePtr tmp_node;
    ostringstream oss;
    string str;
    unsigned short rozlozeni;

    unsigned int mean = 0, dev = 0, min = 0, max = RND_MAX;

                                                // cur_node = victim
    tmp_node = cur_node->xmlChildrenNode;       // tmp_node = ip

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, tmp_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

#ifdef LINUX
    struct hostent *hostent;
    struct in_addr *in;

    if ((hostent = gethostbyname(str.c_str())) == NULL) {
        fprintf(stderr, "ERROR -> Wrong victim IP or name\n\n");
        return ERROR;
    }

    in = (struct in_addr *)hostent->h_addr;

    oss.str("");
    oss.clear();
    oss << inet_ntoa(*in);
#else
#endif
    POSTattack::setVictimIP(oss.str());         // ulozim IP

    tmp_node = tmp_node->next;                  // tmp_node = port

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, tmp_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

#ifdef LINUX
    struct servent *servent;
    if ((servent = getservbyport(htons(strtol(str.c_str(), NULL, 0)), NULL)) == NULL) {
        fprintf(stderr, "ERROR -> Wrong victim port\n\n");
        return ERROR;
    }
#else
#endif
    POSTattack::setVictimPORT( htons(strtol(str.c_str(), NULL, 0)) ); // ulozim port

    cur_node = cur_node->next;                  // cur_node = num_connections

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

    POSTattack::setNumConnections(strtol(str.c_str(), NULL, 0)); // ulozim pocet pripojeni

    cur_node = cur_node->next;                  // cur_node = packet_delay
    tmp_node = cur_node->xmlChildrenNode;       // tmp_node = (const|uniform|exponencial|gaussian)

    /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
    rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
    POSTattack::setTimeDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni casu a jeho parametry

    cur_node = cur_node->next;                  // cur_node = start_delay?, content_length

    if (!xmlStrcmp(cur_node->name, (const xmlChar *)"start_delay")) {  // cur_node = start_delay
        mean = 0; dev = 0; min = 0; max = RND_MAX;
        /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
        rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
        POSTattack::setStartTimeDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni odlozeneho startu a jeho parametry

        cur_node = cur_node->next;              // cur_node = content_length
    }

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

    POSTattack::setContentLength(strtol(str.c_str(), NULL, 0)); // ulozim delku odesilanych dat

    cur_node = cur_node->next;                  // cur_node = data_size_distr
    tmp_node = cur_node->xmlChildrenNode;       // tmp_node = (const|uniform|exponencial|gaussian)

    mean = 0; dev = 0; min = 0; max = RND_MAX;
    /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
    rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
    POSTattack::setDataDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni posilanych dat a jeho parametry

    if (DEBUG_MSG == true) POSTattack::print(); // vypise ulozene parametry

    return SUCCESS;
}


/**
 * Staticka metoda pro parsovani parametru DNS utoku
 * - prochazi XML stromem DNS utoku a parametry nastavuje do
 *   staticke tridy DNS utoku
 * @param   doc       Strom XML dokumentu
 * @param   cur_node  Uzel, na kterem se ve stromu nachazim
 * @return  SUCCESS   Pri uspechu
 * @return  ERROR     Pri chybe
 */
int ParameterParser::parseDNS (xmlDocPtr doc, xmlNodePtr cur_node) {
    xmlNodePtr tmp_node;
    ostringstream oss;
    string str;
    unsigned short rozlozeni;

    unsigned int mean = 0, dev = 0, min = 0, max = RND_MAX;

                                               // cur_node = victim
    tmp_node = cur_node->xmlChildrenNode;      // tmp_node = ip

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, tmp_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

#ifdef LINUX
    struct hostent *hostent;
    struct in_addr *in;

    if ((hostent = gethostbyname(str.c_str())) == NULL) {
        fprintf(stderr, "ERROR -> Wrong victim IP or name\n\n");
        return ERROR;
    }

    in = (struct in_addr *)hostent->h_addr;

    oss.str("");
    oss.clear();
    oss << inet_ntoa(*in);
#else
#endif
    DNSattack::setVictimIP(oss.str());         // ulozim IP

    tmp_node = tmp_node->next;                 // tmp_node = port

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, tmp_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

#ifdef LINUX
    struct servent *servent;
    if ((servent = getservbyport(htons(strtol(str.c_str(), NULL, 0)), NULL)) == NULL) {
        fprintf(stderr, "ERROR -> Wrong victim port\n\n");
        return ERROR;
    }
#else
#endif
    DNSattack::setVictimPORT( htons(strtol(str.c_str(), NULL, 0)) ); // ulozim port

    /* projde vsechny uzly "dns_ip" */
    for (cur_node = cur_node->next; !xmlStrcmp(cur_node->name, (const xmlChar *)"dns_ip"); cur_node = cur_node->next) {
        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

#ifdef LINUX
        if ((hostent = gethostbyname(str.c_str())) == NULL) {
            if (DEBUG_MSG == true) fprintf(stderr, "WARNING -> Wrong DNS IP or name\n\n");
        } else {
            in = (struct in_addr *)hostent->h_addr;

            oss.str("");
            oss.clear();
            oss << inet_ntoa(*in);

            DNSattack::addDNS(oss.str());      // do seznamu vlozim IP adresy DNS serveru
        }
#else
#endif
    }

    if (DNSattack::emptyDNS()) {
        fprintf(stderr, "ERROR -> No correct DNS IP or name\n\n");
        return ERROR;
    }

                                               // cur_node = packet_delay
    tmp_node = cur_node->xmlChildrenNode;      // tmp_node = (const|uniform|exponencial|gaussian)

    /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
    rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
    DNSattack::setTimeDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni casu a jeho parametry

    cur_node = cur_node->next;                 // cur_node = start_delay?, dns_q_url

    if (!xmlStrcmp(cur_node->name, (const xmlChar *)"start_delay")) { // cur_node = start_delay
        mean = 0; dev = 0; min = 0; max = RND_MAX;
        /* zparsuje pozadovane rozlozeni, vrati jeho nazev a nastavi potrebne promenne */
        rozlozeni = ParameterParser::parseDistr(doc, tmp_node, &mean, &dev, &min, &max);
        DNSattack::setStartTimeDistr(rozlozeni, mean, dev, min, max); // ulozim rozlozeni odlozeneho startu a jeho parametry

        cur_node = cur_node->next;              // cur_node = dns_q_url
    }

    oss.str("");
    oss.clear();
    oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
    str = oss.str();
    trim(str);

    DNSattack::setQueryURL(str);               // ulozim URL pro DNS dotaz

    if (DEBUG_MSG == true) DNSattack::print(); // vypise ulozene parametry

    return SUCCESS;
}


/**
 * Staticka metoda zjisti o jakou distribucni funkci se jedna a vrati jeji kod a parametry
 * - parametry vraci pomoci promennych predanych odkazem
 * - pokud rozlozeni nektery z parametru nepouziva, jeho hodnota se nemeni
 * - pokud je zadano konstantni rozlozeni, jeho hodnota se vraci v promenne "mean"
 * - pokud je zadane minimum vetsi nez maximum, jejich hodnoty se prohodi
 * @param   doc             Strom XML dokumentu
 * @param   cur_node        Uzel, na kterem se ve stromu nachazim
 * @param  *mean            Ukazatel na stredni hodnotu
 * @param  *dev             Ukazatel na smerodatnou odchylku
 * @param  *min             Ukazatel na minimum
 * @param  *max             Ukazatel na maximum
 * @return  unsigned short  Kod rozlozeni - definovane v DoSlib.hpp
 */
unsigned short ParameterParser::parseDistr (xmlDocPtr doc, xmlNodePtr cur_node, unsigned int* mean, unsigned int* dev, unsigned int* min, unsigned int* max) {
    ostringstream oss;
    string distr_type;
    string str;
    unsigned short distr_type_int;

    oss << xmlStrdup(cur_node->name);
    distr_type = oss.str();

    if (distr_type == "const") {              // const
        distr_type_int = DISTR_CONST;

        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

        *mean = strtol(str.c_str(), NULL, 0);

    }
    else if (distr_type == "uniform") {       // uniform
        distr_type_int = DISTR_UNIFORM;

        cur_node = cur_node->xmlChildrenNode; // cur_node = min

        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

        *min = strtol(str.c_str(), NULL, 0);

        cur_node = cur_node->next;            // cur_node = max

        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

        *max = strtol(str.c_str(), NULL, 0);

    }
    else if (distr_type == "exponencial") {   // exponencial
        distr_type_int = DISTR_EXPONENCIAL;

        cur_node = cur_node->xmlChildrenNode; // cur_node = mean

        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

        *mean = strtol(str.c_str(), NULL, 0);

        cur_node = cur_node->next;            // cur_node = min?, max?, NULL

        if (cur_node) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"min")) {
                oss.str("");
                oss.clear();
                oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
                str = oss.str();
                trim(str);

                *min = strtol(str.c_str(), NULL, 0);
            } else {
                oss.str("");
                oss.clear();
                oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
                str = oss.str();
                trim(str);

                *max = strtol(str.c_str(), NULL, 0);
            }

            cur_node = cur_node->next;        // cur_node = max?, NULL

            if (cur_node) {
                oss.str("");
                oss.clear();
                oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
                str = oss.str();
                trim(str);

                *max = strtol(str.c_str(), NULL, 0);
            }
        }

    }
    else if (distr_type == "gaussian") {      // gaussian
        distr_type_int = DISTR_GAUSSIAN;

        cur_node = cur_node->xmlChildrenNode; // cur_node = mean

        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

        *mean = strtol(str.c_str(), NULL, 0);

        cur_node = cur_node->next;            // cur_node = deviation

        oss.str("");
        oss.clear();
        oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
        str = oss.str();
        trim(str);

        *dev = strtol(str.c_str(), NULL, 0);

        cur_node = cur_node->next;            // cur_node = min?, max?, NULL

        if (cur_node) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"min")) {
                oss.str("");
                oss.clear();
                oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
                str = oss.str();
                trim(str);

                *min = strtol(str.c_str(), NULL, 0);
            } else {
                oss.str("");
                oss.clear();
                oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
                str = oss.str();
                trim(str);

                *max = strtol(str.c_str(), NULL, 0);
            }

            cur_node = cur_node->next;        // cur_node = max?, NULL

            if (cur_node) {
                oss.str("");
                oss.clear();
                oss << xmlNodeListGetString(doc, cur_node->xmlChildrenNode, 1);
                str = oss.str();
                trim(str);

                *max = strtol(str.c_str(), NULL, 0);
            }
        }
    }

    if (*min > *max) {
        unsigned int tmp;

        tmp  = *min;
        *min = *max;
        *max = tmp;
    }

    return distr_type_int;
}
