///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    DNSattack.hpp                                              ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////
/**
 * Implementace vytvorena za pomoci navodu dostupneho z:
 * http://www.binarytides.com/raw-udp-sockets-c-linux/
 */

#ifndef DNSATTACK_HPP_INCLUDED
#define DNSATTACK_HPP_INCLUDED

#include "DoSlib.hpp"

using namespace std;

/**
 * Staticka trida DNS utoku
 */
class DNSattack: public std::enable_shared_from_this<DNSattack> {
    public:
        /**
         * Staticka metoda nastavi parametry utoku na defaultni hodnoty
         * - je potreba zadat pouze IP obeti
         * @param  victim_ip  IP adresa obeti
         */
        static void setDefault (string victim_ip);

        /**
         * Staticka metoda nastavi IP obeti
         * - take nastavi utok jako aktivni
         * @param  victim_ip  IP adresa obeti
         */
        static void setVictimIP (string victim_ip);

        /**
         * Staticka metoda nastavi port obeti
         * @param  port  Port obeti
         */
        static void setVictimPORT (unsigned short port = DEF_PORT);

        /**
         * Staticka metoda prida IP mezi IP adresy DNS serveru
         * @param  dns_ip  IP adresa DNS serveru
         */
        static void addDNS(string dns_ip);

        /**
         * Metoda nastavi IP adresu DNS serveru
         * @param  dns_ip  IP adresa DNS serveru
         */
        void setDNS (string dns_ip);

        /**
         * Staticka metoda zjisti zda je seznam DNS serveru prazdny
         * @return  1  Pokud je prazdny
         * @return  0  Pokud neni prazdny
         */
        static int emptyDNS();

        /**
         * Staticka metoda nastavi URL pro DNS dotaz
         * @param  dns_q_url  URL pro DNS dotaz
         */
        static void setQueryURL (string dns_q_url = DEF_DNS_Q_URL);

        /**
         * Staticka metoda nastavi casove rozlozeni a jeho parametry
         * - nastavi take rozlozeni odlozeneho startu pro pripad, ze nebude zadano
         * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
         * @param  time_distr  Kod rozlozeni - definovane v DoSlib.hpp
         * @param  time_mean   Stredni hodnota
         * @param  time_dev    Smerodatna odchylka
         * @param  time_min    Minimum
         * @param  time_max    Maximum
         */
        static void setTimeDistr (unsigned short time_distr = DEF_TIME_DISTR, unsigned int time_mean = DEF_TIME_MEAN, unsigned int time_dev = DEF_TIME_DEV, unsigned int time_min = DEF_TIME_MIN, unsigned int time_max = DEF_TIME_MAX);

        /**
         * Staticka metoda nastavi rozlozeni odlozeneho startu a jeho parametry
         * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
         * @param  start_time_distr  Kod rozlozeni - definovane v DoSlib.hpp
         * @param  start_time_mean   Stredni hodnota
         * @param  start_time_dev    Smerodatna odchylka
         * @param  start_time_min    Minimum
         * @param  start_time_max    Maximum
         */
        static void setStartTimeDistr (unsigned short start_time_distr = DEF_START_TIME_DISTR, unsigned int start_time_mean = DEF_START_TIME_MEAN, unsigned int start_time_dev = DEF_START_TIME_DEV, unsigned int start_time_min = DEF_START_TIME_MIN, unsigned int start_time_max = DEF_START_TIME_MAX);

        /**
         * Staticka metoda vypise parametry utoku
         */
        static void print ();

        /**
         * Staticka startovaci metoda pripravi a spusti utok
         * - vytvori objekty utoku a naplanuje jejich spusteni (naplni jimi kalendar udalosti)
         * - invokaci "AttackControl::run()" spusti rizeni utoku
         */
        static void start ();

        /**
         * Staticka ukoncovaci metoda
         * - pouziva se pri zachyceni signalu
         * - nastavi utok jako neaktivni
         * - vymaze kalendar udalosti a zaridi, ze se do nej uz nic nevlozi
         * - pote se necha program dobehnout
         */
        static void end ();

        /**
         * Staticka metoda vraci zda je utok aktivni
         * @return  1  Pokud je aktivni
         * @return  0  Pokud neni aktivni
         */
        static int isActive ();

    private:
        /**
         * Metoda vytvori socket
         * - naplanuje invokaci metody pro odeslani DNS pozadavku
         * - pokud vytvareni socketu selze, neplanuje nic
         */
        void sockOpen ();

        /**
         * Metoda odesle DNS pozadavek s podvrhnutou IP odesilatele
         * - sama sebe naplanuje do kalendare
         */
        void sendDNSmessage ();

        /**
         * Metoda uzavre spojeni s obeti
         */
        void sockClose ();

        /**
         * Metoda naplanuje invokaci metody volajiciho objektu do kalendare udalosti
         * @param  event  Wrapper planovane udalosti
         * @param  type   Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
         */
        void planMe (std::function<void()> event, int type = PACKET_DELAY);

        /**
         * Metoda vrati ukazatel na sebe
         * - jako "this", ale vraci shared_ptr
         * @return  std::shared_ptr<DNSattack>  Ukazatel na sebe
         */
        std::shared_ptr<DNSattack> getPtr ();

        /**
         * Metoda naplni datagram
         * datagram vyplnuje v promenne predane odkazem
         * - vyplni IP hlavicku, UDP halvicku, ziska DNS pozadavek,
         *   spocita kontrolni soucty
         * @param  *datagram  Ukazatel na prazdny datagram
         * @return  int       Delka datagramu
         */
        int getDatagram (char *datagram);

        /**
         * Metoda vrati DNS pozadavek
         * pozadavek vyplnuje v promenne predane odkazem
         * @param  *dns_message  Ukazatel na prazdny pozadavek
         * @return  int          Delka pozadavku
         */
        int getDNSmessage (char *dns_message);

        /**
         * Metoda vrati adresu pro prelozeni ve tvaru prijimanem DNS
         * adresu vyplnuje v promenne predane odkazem
         * www.google.com -> [0x03]www[0x06]google[0x03]com[0x00]
         * @param  *dns_address  Ukazatel na prazdnou adresu
         * @param   address      Adresa pro prelozeni v klasickem tvaru
         * @return  int          Delka adresy
         */
        int getDNSaddress (char *dns_address, string address);

        /**
         * Staticka metoda vrati nahodne cislo
         * - parametrem lze zvolit jake nahodne cislo chceme
         * @param   type          Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
         * @return  unsigned int  Pseudo-nahodne cislo
         */
        static unsigned int getRandom (int type = PACKET_DELAY);

        /**
         * Staticka metoda vrati nahodne cislo
         * - pro velikost nahodneho retezce v HTTP hlavicce
         * @param   min           Minimum
         * @param   max           Maximum
         * @return  unsigned int  Pseudo-nahodne cislo
         */
        static unsigned int getRandom (int min, int max);

        /**
         * Staticka metoda vrati aktivacni cas udalost
         * - vygeneruje si nahodne cislo
         * - pricte ho k aktualnimu casu
         * @param   type     Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
         * @return  timeval  Aktivacni cas udalosti
         */
        static struct timeval getActTime (int type = PACKET_DELAY);

        /**
         * Deklarace promennych a datovych struktur
         */
        static bool active;                      // staticka promenna znaci zda je utok aktivni

        static string victim_ip;                 // staticka promenna uchovavajici IP adresu obeti
        static unsigned short victim_port;       // staticka promenna uchovavajici PORT obeti
        static vector<string> dns_ip_vec;        // staticky seznam uchovavajici IP adresy DNS serveru
        static unsigned int dns_count;           // staticka promenna pri vkladani uchovava pocet DNS serveru v seznamu, pozdeji slouzi jako index do seznamu
        static string dns_q_url;                 // staticka promenna uchovavajici URL pro DNS dotaz

        static unsigned short time_distr;        // staticka promenna uchovavajici kod casoveho rozlozeni - definovane v DoSlib.hpp
        static unsigned int time_mean;           // staticka promenna uchovavajici stredni hodnotu rozlozeni
        static unsigned int time_dev;            // staticka promenna uchovavajici odchylku rozlozeni
        static unsigned int time_min;            // staticka promenna uchovavajici minimalni hodnotu rozlozeni
        static unsigned int time_max;            // staticka promenna uchovavajici maximalni hodnotu rozlozeni

        static unsigned short start_time_distr;  // staticka promenna uchovavajici kod casoveho rozlozeni - definovane v DoSlib.hpp
        static unsigned int   start_time_mean;   // staticka promenna uchovavajici stredni hodnotu rozlozeni
        static unsigned int   start_time_dev;    // staticka promenna uchovavajici odchylku rozlozeni
        static unsigned int   start_time_min;    // staticka promenna uchovavajici minimalni hodnotu rozlozeni
        static unsigned int   start_time_max;    // staticka promenna uchovavajici maximalni hodnotu rozlozeni

        static std::shared_ptr<CalendarOfEvents> calendar_of_events;      // staticky kalendar udalosti pro DNS utok
        static std::shared_ptr<CalendarOfEvents> calendar_of_end_events;  // staticky kalendar ukoncovacich udalosti pro DNS utok
        static std::shared_ptr<RandomGenerator>  random_generator;        // staticky generator pseudo-nahodnych cisel

        int sockfd = SOCK_ERROR;                 // socket file descriptor
        string dns_ip = "172.0.0.1";             // DNS IP pro konkretni instanci utoku
};

#endif // DNSATTACK_HPP_INCLUDED
