///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    RandomGenerator.cpp                                        ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojovy kod, jakozto i cely tento nastroj           ///
///                je dostupny pod OPEN SOURCE licenci VUT V BRNE           ///
///              Uplne zneni licence dostupne v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#include "RandomGenerator.hpp"

#include <sys/time.h>
#include <algorithm>
#include <limits.h>
#include <math.h>


/**
 * Konstruktor tridy RandomGenerator
 * - pro zajisteni nahodnosti se seminko defaultne spocita z funkce casu
 * @param  multiplier  Nasobitel nahodneho cisla
 * @param  increment   Prirustek nahodneho cisla
 */
RandomGenerator::RandomGenerator (unsigned int multiplier, unsigned int increment) {
    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    this->random_number = tv_now.tv_usec;  // nastavi seminko
    this->multiplier = multiplier;         // nastavi nasobitel nahodneho cisla
    this->increment  = increment;          // nastavi prirustek nahodneho cisla
}


/**
 * Metoda pro nastaveni seminka
 * @param  seed  Seminko (poratecni hodnota generovanych cisel)
 */
void RandomGenerator::setSeed (unsigned int seed) {
    this->random_number = seed;
}


/**
 * Spocita dalsi pseudonahodne cislo
 * - z intervalu  <0, RAND_MAX)
 * - pracuje na principu linearniho kongruentniho generatoru
 * @return  double  Pseudonahodne cislo z intervalu <0, RAND_MAX)
 */
unsigned int RandomGenerator::getNext () {
    random_number = random_number * multiplier + increment;  // ulozi spocitane cislo pro dalsi vypocet
    return random_number;                                    // vrati spocitane cislo
}


/**
 * Generuje normalizovana pseudonahodna cisla rovnomerneho rozlozeni
 * - v intervalu <0, 1)
 * @return  double  Pseudonahodne cislo z intervalu <0, 1)
 */
double RandomGenerator::normalized () {
    return getNext() / (UINT_MAX + 1.0);
}


/**
 * Generuje pseudonahodna cisla rovnomerneho rozlozeni
 * - ve zvolenem intervalu <min_val, max_val)
 * @param   min_val  Dolni mez intervalu
 * @param   max_val  Horni mez intervalu
 * @return  double   Pseudonahodne cislo z intervalu <min_val, max_val)
 */
double RandomGenerator::uniform (double min_val, double max_val) {
    // horni mez intervalu nemuze byt mensi nez dolni mez
    if (min_val >= max_val)
        std::swap(min_val,max_val);

    // ziska normalizovane pseudonahodne cislo a prepocita ho do pozadovaneho intervalu
    return min_val + (max_val - min_val) * normalized();
}


/**
 * Generuje pseudonahodna cisla exponencialniho rozlozeni
 * - pouziva metodu inverzni transformace
 * @param   mean    Stredni hodnota
 * @return  double  Pseudonahodne cislo
 */
double RandomGenerator::exponencial (double mean) {
    // ziska normalizovane pseudonahodne cislo a transformuje ho pozadovaneho "intervalu"
    return -mean * log(1.0 - normalized());
}


/**
 * Generuje pseudonahodna cisla gaussova (normalniho) rozlozeni
 * - funguje na tom principu, ze pokud secteme 12 pseudonahodnych cisel
 *   z intervalu <0, 1) a od souctu odecteme cislo 6, tak dostaneme pseudonahodne
 *   cislo gaussova rozlozeni v intervalu <0, 1)
 * - pote toto cislo prepocitame do pozadovaneho "intervalu"
 * @param   mean       Stredni hodnota
 * @param   deviation  Smerodatna odchylka
 * @return  double     Pseudonahodne cislo
 */
double RandomGenerator::gaussian (double mean, double deviation) {
    double sum = 0.0;

    // secteme 12 normalizovanych pseudonahodnych cisel
    for (int i = 0; i < 12; ++i)
        sum += normalized();

    // od souctu odecteme cislo 6 a prepocitame do pozadovaneho "intervalu"
    return (sum - 6.0) * deviation + mean;
}
