///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    GETattack.hpp                                              ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#ifndef GETATTACK_HPP_INCLUDED
#define GETATTACK_HPP_INCLUDED

#include "DoSlib.hpp"

using namespace std;

/**
 * Staticka trida GET utoku
 */
class GETattack: public std::enable_shared_from_this<GETattack> {
    public:
        /**
         * Staticka metoda nastavi parametry utoku na defaultni hodnoty
         * - je potreba zadat pouze IP obeti
         * @param  victim_ip  IP adresa obeti
         */
        static void setDefault (string victim_ip);

        /**
         * Staticka metoda nastavi IP obeti
         * - take nastavi utok jako aktivni
         * @param  victim_ip  IP adresa obeti
         */
        static void setVictimIP (string victim_ip);

        /**
         * Staticka metoda nastavi port obeti
         * @param  port  Port obeti
         */
        static void setVictimPORT (unsigned short port = DEF_PORT);

        /**
         * Staticka metoda nastavi pozadovany pocet pripojeni
         * @param  num_connections  Pocet pripojeni
         */
        static void setNumConnections (unsigned int num_connections = DEF_NUM_CONNECTIONS);

        /**
         * Staticka metoda nastavi casove rozlozeni a jeho parametry
         * - nastavi take rozlozeni odlozeneho startu pro pripad, ze nebude zadano
         * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
         * @param  time_distr  Kod rozlozeni - definovane v DoSlib.hpp
         * @param  time_mean   Stredni hodnota
         * @param  time_dev    Smerodatna odchylka
         * @param  time_min    Minimum
         * @param  time_max    Maximum
         */
        static void setTimeDistr (unsigned short time_distr = DEF_TIME_DISTR, unsigned int time_mean = DEF_TIME_MEAN, unsigned int time_dev = DEF_TIME_DEV, unsigned int time_min = DEF_TIME_MIN, unsigned int time_max = DEF_TIME_MAX);

        /**
         * Staticka metoda nastavi rozlozeni odlozeneho startu a jeho parametry
         * - nastavi se i hodnoty, ktere rozlozeni nepouziva, pozdeji se proste nepouziji
         * @param  start_time_distr  Kod rozlozeni - definovane v DoSlib.hpp
         * @param  start_time_mean   Stredni hodnota
         * @param  start_time_dev    Smerodatna odchylka
         * @param  start_time_min    Minimum
         * @param  start_time_max    Maximum
         */
        static void setStartTimeDistr (unsigned short start_time_distr = DEF_START_TIME_DISTR, unsigned int start_time_mean = DEF_START_TIME_MEAN, unsigned int start_time_dev = DEF_START_TIME_DEV, unsigned int start_time_min = DEF_START_TIME_MIN, unsigned int start_time_max = DEF_START_TIME_MAX);

        /**
         * Staticka metoda vypise parametry utoku
         */
        static void print ();

        /**
         * Staticka startovaci metoda pripravi a spusti utok
         * - vytvori objekty utoku a naplanuje jejich spusteni (naplni jimi kalendar udalosti)
         * - invokaci "AttackControl::run()" spusti rizeni utoku
         */
        static void start ();

        /**
         * Staticka ukoncovaci metoda
         * - pouziva se pri zachyceni signalu
         * - nastavi utok jako neaktivni
         * - vymaze kalendar udalosti a zaridi, ze se do nej uz nic nevlozi
         * - pote se necha program dobehnout
         */
        static void end ();

        /**
         * Staticka metoda vraci zda je utok aktivni
         * @return  1  Pokud je aktivni
         * @return  0  Pokud neni aktivni
         */
        static int isActive ();

    private:
        /**
         * Metoda vytvori socket
         * - naplanuje invokaci metody pro pripojeni k obeti
         * - pokud vytvareni socketu selze, neplanuje nic
         */
        void sockOpen ();

        /**
         * Pripoji se k obeti
         * - naplanuje invokaci dalsi metody v posloupnosti provadeni utoku
         * - pokud pripojeni selze, naplanuje znovu invokaci teto metody
         */
        void victimConnect ();

        /**
         * Posle uvodni cast HTTP GET hlavicky
         * - dalsi fazi utoku naplanuje do kalendare
         * - pri chybe naplanuje do kalendare pripojeni k obeti
         */
        void sendFrstHeaderPart ();

        /**
         * Posle dalsi cast HTTP GET hlavicky
         * - sama sebe naplanuje do kalendare
         * - pri HTTP GET 4xx, 5xx naplanuje do kalendare prvni cast utoku
         * - pri chybe naplanuje do kalendare pripojeni k obeti
         */
        void sendNextHeaderPart ();

        /**
         * Zkontroluje, zda obet neodpovedela chybovym kodem
         * - HTTP GET kod 4xx, 5xx
         * @param  *http_response  Odpoved obeti
         * @return  0              Pokud ne
         * @return  1              Pokud ano
         */
        int checkForError (char *http_response);

        /**
         * Metoda uzavre spojeni s obeti
         */
        void sockClose ();

        /**
         * Metoda naplanuje invokaci metody volajiciho objektu do kalendare udalosti
         * @param  event  Wrapper planovane udalosti
         * @param  type   Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
         */
        void planMe (std::function<void()> event, int type = PACKET_DELAY);

        /**
         * Metoda vrati ukazatel na sebe
         * - jako "this", ale vraci shared_ptr
         * @return  std::shared_ptr<GETattack>  Ukazatel na sebe
         */
        std::shared_ptr<GETattack> getPtr ();

        /**
         * Metoda vrati prvni cast HTTP GET hlavicky
         * - ve vylepsene verzi by se mohli parametry randomovat
         * @return  string  Cast HTTP GET hlavicky
         */
        string getFrstHeaderPart ();

        /**
         * Metoda vrati dalsi cast HTTP GET hlavicky
         * - ve vylepsene verzi by se mohlo randomovat aj neco rozumnyho
         *   (pravy parametr, random hodnota, nebo tak neco)
         * @return  string  Cast HTTP GET hlavicky
         */
        string getNextHeaderPart ();

        /**
         * Staticka metoda vrati nahodne cislo
         * - parametrem lze zvolit jake nahodne cislo chceme
         * @param   type          Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
         * @return  unsigned int  Pseudo-nahodne cislo
         */
        static unsigned int getRandom (int type = PACKET_DELAY);

        /**
         * Staticka metoda vrati nahodne cislo
         * - pro velikost nahodneho retezce v HTTP hlavicce
         * @param   min           Minimum
         * @param   max           Maximum
         * @return  unsigned int  Pseudo-nahodne cislo
         */
        static unsigned int getRandom (int min, int max);

        /**
         * Staticka metoda vrati aktivacni cas udalost
         * - vygeneruje si nahodne cislo
         * - pricte ho k aktualnimu casu
         * @param   type     Typ pozadovaneho nahodneho cisla - typy definovane v DoSlib.hpp
         * @return  timeval  Aktivacni cas udalosti
         */
        static struct timeval getActTime (int type = PACKET_DELAY);

        /**
         * Deklarace promennych a datovych struktur
         */
        static bool active;                      // staticka promenna znaci zda je utok aktivni

        static string victim_ip;                 // staticka promenna uchovavajici IP adresu obeti
        static unsigned short victim_port;       // staticka promenna uchovavajici PORT obeti
        static unsigned int num_connections;     // staticka promenna uchovavajici pozadovany pocet pripojeni

        static unsigned short time_distr;        // staticka promenna uchovavajici kod casoveho rozlozeni - definovane v DoSlib.hpp
        static unsigned int   time_mean;         // staticka promenna uchovavajici stredni hodnotu rozlozeni
        static unsigned int   time_dev;          // staticka promenna uchovavajici odchylku rozlozeni
        static unsigned int   time_min;          // staticka promenna uchovavajici minimalni hodnotu rozlozeni
        static unsigned int   time_max;          // staticka promenna uchovavajici maximalni hodnotu rozlozeni

        static unsigned short start_time_distr;  // staticka promenna uchovavajici kod casoveho rozlozeni - definovane v DoSlib.hpp
        static unsigned int   start_time_mean;   // staticka promenna uchovavajici stredni hodnotu rozlozeni
        static unsigned int   start_time_dev;    // staticka promenna uchovavajici odchylku rozlozeni
        static unsigned int   start_time_min;    // staticka promenna uchovavajici minimalni hodnotu rozlozeni
        static unsigned int   start_time_max;    // staticka promenna uchovavajici maximalni hodnotu rozlozeni

        static std::shared_ptr<CalendarOfEvents> calendar_of_events;      // staticky kalendar udalosti pro GET utok
        static std::shared_ptr<CalendarOfEvents> calendar_of_end_events;  // staticky kalendar ukoncovacich udalosti pro GET utok
        static std::shared_ptr<RandomGenerator>  random_generator;        // staticky generator pseudo-nahodnych cisel

        int sockfd = SOCK_ERROR;                 // socket file descriptor
};

#endif // GETATTACK_HPP_INCLUDED
