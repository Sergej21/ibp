///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    DoSlib.hpp                                                 ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#ifndef DOS_LIBRARY_INCLUDED
#define DOS_LIBRARY_INCLUDED

/**
 * Makro pro debugovaci vypisy
 */
#ifdef  DEBUG
    #define DEBUG_MSG true
#else
    #define DEBUG_MSG false
#endif

// konstanty pro navratove hodnoty
#define SUCCESS            0
#define ERROR              1
#define HELP               2

// konstanty kodu jednotlivich rozlozeni (trida ...)
#define DISTR_CONST        0       // const
#define DISTR_UNIFORM      1       // uniform
#define DISTR_EXPONENCIAL  2       // exponencial
#define DISTR_GAUSSIAN     3       // gaussian

// konstanty pro urceni pozadovaneho nahodneho cisla
#define ZERO               0       // vrati vynulovanou strukturu
#define START_DELAY        1       // odlozeny start
#define PACKET_DELAY       2       // cas mezi packety
#define DATA_DISTR         3       // velikosti dat

// konstanty pro generovani ASCII zanku
#define ASC_MIN           64
#define ASC_MAX          122 - 6

// konstanty pro vytvareni DNS pozadavku
#define DATAGRAM_SIZE           4096
#define DNS_QUERY_SIZE          2048
#define DNS_URL_SIZE            1024
#define _16b_ID_MIN                0
#define _16b_ID_MAX            65535
#define DNS_SRV_PORT              53
#define DNS_FLAG_RECURSE      0x0100
#define DNS_TYPE_A                 1
#define DNS_TYPE_NS                2
#define DNS_TYPE_CNAME             5
#define DNS_TYPE_SOA               6
#define DNS_TYPE_WKS              11
#define DNS_TYPE_PTR              12
#define DNS_TYPE_MX               15
#define DNS_TYPE_SRV              33
#define DNS_TYPE_A6               38
#define DNS_TYPE_ANY             255
#define DNS_CLASS_IN               1

// nejake dalsi konstanty
#define RESP_WAIT_MS               5  // 5ms
#define HTTP_BUF_SIZE           1024
#define SOCK_ERROR                -1
#define BUFFER                   256
#define USEC_MOD             1000000
#define RND_MAX           1800000000  // 30min / 1.8GB

// retezcove konstanty pro pozadavky - mohou byt vymeneny za nejakou fci co je bude randomovat
#define USER_AGENT "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 7.0; InfoPath.3; .NET CLR 3.1.40767; Trident/6.0; en-IN)"

// defaultni konstanty, pokud utok nebyl nastaven pomoci XML
#define DEF_PORT                  80
#define DEF_NUM_CONNECTIONS      250
#define DEF_CONTENT_LENGTH   1000000  // 1MB

#define DEF_TIME_DISTR        DISTR_EXPONENCIAL
#define DEF_TIME_MEAN            250
#define DEF_TIME_DEV               0
#define DEF_TIME_MIN               0
#define DEF_TIME_MAX          RND_MAX

#define DEF_START_TIME_DISTR  DISTR_GAUSSIAN
#define DEF_START_TIME_MEAN   150000          // 2.5min
#define DEF_START_TIME_DEV    120000          // +- 2min
#define DEF_START_TIME_MIN         0
#define DEF_START_TIME_MAX    300000          // 5min

#define DEF_DATA_DISTR        DISTR_CONST
#define DEF_DATA_MEAN            256          // 256b / 64znaku
#define DEF_DATA_DEV               0
#define DEF_DATA_MIN               0
#define DEF_DATA_MAX               0

#define DEF_DNS_Q_URL "root-servers.net"

// Importovani standardnich knihoven
#include <csignal>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <string>
#include <vector>
//#include <err.h>

// Importovani smart pointeru
#include <memory>

// Importovani wrapperu funkci/metod
#include <functional>

// Importovani prace s casem
#include <sys/time.h>

// Importovani prace s vlaknama
#ifdef LINUX
    #include <pthread.h>
    #include <semaphore.h>
#else
#endif

// Importovani prace se siti
#ifdef LINUX
    #include <netdb.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <arpa/inet.h>

    #include <netinet/in.h>
    #include <netinet/ip.h>
    #include <netinet/udp.h>
#else
    #include <winsock2.h>
    #include <windows.h>

/*
 * Copyright (C) 2010 Franz Leitl, Embedded Systems Lab
 */
// HACK ugly hack...
#   ifndef timeradd
// Implement the function timeradd
#       define timeradd(a, b, result)                        \
            (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;    \
            (result)->tv_usec = (a)->tv_usec + (b)->tv_usec; \
            if ((result)->tv_usec >= 1000000) {              \
                ++(result)->tv_sec;                          \
                (result)->tv_usec -= 1000000;                \
            }                                                \

#   endif

// HACK ugly hack...
// Implement the function timersub
#   ifndef timersub
#       define timersub(a, b, result) \
            (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;    \
            (result)->tv_usec = (a)->tv_usec - (b)->tv_usec; \
            if ((result)->tv_usec < 0) {                     \
                --(result)->tv_sec;                          \
                (result)->tv_usec += 1000000;                \
            }                                                \

#   endif
#endif
/*
 * END of Copyright (C) 2010 Franz Leitl, Embedded Systems Lab
 */

// Importovani generatoru pseudo-nahodnych cisel
#include "RandomGenerator.hpp"


// Definice struktur pro odeslani DNS dotazu
/**
 * UDP pseudo-hlavicka pro kontrolni soucet (12B)
 */
struct udpphdr {
    uint32_t saddr;
    uint32_t daddr;
    uint8_t  zeros;
    uint8_t  protocol;
    uint16_t udp_len;
};

/**
 * DNS hlavicka (12B)
 */
struct dnshdr {
    uint16_t id;
    uint16_t flags;
    uint16_t qd_count;
    uint16_t an_count;
    uint16_t ns_count;
    uint16_t ar_count;
};

/**
 * Konec DNS query (4B)
 */
struct dnstail {
    uint16_t _type;
    uint16_t _class;
};


// Hlavicky funkci z mainu pouzivanych v tridach
/**
 * Spocita kontrolni soucet
 *  - funkce s drobnymi upravami prevzata z:
 *    http://www.binarytides.com/raw-udp-sockets-c-linux/
 * @param  *struct_ptr      Ukazatel na strukturu, jejiz kontrolni soucet pocitam
 * @param   len             Delka struktury
 * @return  unsigned short  Kontrolni soucet
 */
unsigned short checksum(unsigned short *struct_ptr, int len);

/**
 * Orizne bile znaky
 * - z predaneho retezce orizne pocatecni a koncove bile znaky
 * @param  &str  Retezec pro orezani
 */
void trim (std::string& str);

/**
 * Vrati nahodny retezec o zvolene velikosti
 * @param   len     Pozadovana velikost
 * @return  string  Nahodny retezec
 */
std::string getRandStr (int len);

/**
 * Vrati aktualni cas v pozadovanem formatu
 * @return  string  Retezec s casem
 */
std::string getTime ();

/**
 * Vrati predany cas v pozadovanem formatu
 * @param   tv_time  Cas pro prevod na retezec
 * @return  string   Retezec s casem
 */
std::string getTime (struct timeval tv_time);

/**
 * Signal Handler
 * @param  signum  Cislo odchyceneho signalu
 */
void signalHandler (int signum);


// Importovani vsech hlavickovych souboru pro praci s knihovnou
#include "ParameterParser.hpp"
#include "CalendarOfEvents.hpp"
#include "AttackControl.hpp"
#include "GETattack.hpp"
#include "POSTattack.hpp"
#include "DNSattack.hpp"

#endif // DOS_LIBRARY_INCLUDED
