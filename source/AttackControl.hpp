///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    AttackControl.hpp                                          ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#ifndef ATTACKCONTROL_HPP_INCLUDED
#define ATTACKCONTROL_HPP_INCLUDED

#include "DoSlib.hpp"

using namespace std;

/**
 * Staticka trida ovladani utoku
 */
class AttackControl {
    public:
        /**
         * Staticka metoda ovlada provadeni utoku
         * - provadi "next event" algoritmus
         * - dokud neni kalendar udalosti prazdny,
         *   vytahne prvni zaznam z kalendare,
         *   pocka na jeho aktivacni cas a invokuje danou metodu
         * @param  calendar_of_events  Kalendar udalosti, ktery ma prochazet
         * @param  attack_name         Jmeno utoku, ktery metodu invokoval
         */
        static void run (std::shared_ptr<CalendarOfEvents> calendar_of_events, string attack_name = "");

    private:
        /**
         * Staticka metoda pro uspani do predaneho casu
         * - pokud je aktuani cas vetsi, nez predany cas, neuspava se
         * @param  tv_act_time  Cas probuzeni
         */
        static void goSleep (struct timeval tv_act_time);
};

#endif // ATTACKCONTROL_HPP_INCLUDED
