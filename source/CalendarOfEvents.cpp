///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    CalendarOfEvents.cpp                                       ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#include "DoSlib.hpp"

using namespace std;


/**
 * Metoda pro vlozeni aktivacniho zaznamu do kalendare
 * - kalendar je vzdy serazeny podle aktivacniho casu
 * - pri vkladani hleda a vklada na spravne misto
 * - vklada pouze pokud je kalendar aktivni
 * @param  act_rec  Objekt ActivationRecord obsahujici aktivacni zaznam
 */
void CalendarOfEvents::add (std::shared_ptr<ActivationRecord> act_rec) {
    if (this->active) { // pokud je kalendar aktivni
        // iterator pres aktivacni zaznamy
        auto it = begin (this->activation_records);

        // prochazime aktivacnimi zaznamy a hledame spravne misto, kam vlozit aktivacni zaznam
        for (; it != end (this->activation_records); ++it) {
            // pokud je aktivacni cas vkladaneho zaznamu mensi nez cas zaznamu z kalendare
            if (timercmp(&(act_rec->tv_act_time), &((*it)->tv_act_time), <)) {
                // ukoncime hledani a vlozime zaznam do kalendare zaznamu
                break;
            }
        }

        // vlozime aktivacni zaznam na spravne misto
        this->activation_records.insert(it, act_rec);
    }
}


/**
 * Metoda vrati prvni aktivacni zaznam a odstrani ho z kalendare
 * @return  std::shared_ptr<ActivationRecord>  Aktivacni zaznam udalosti
 * @return  NULL                               Pokud byl kalendar prazdny
 */
std::shared_ptr<ActivationRecord> CalendarOfEvents::popFront () {
    if (this->activation_records.empty())
        return NULL;

    std::shared_ptr<ActivationRecord> act_rec = this->activation_records.front();

    this->activation_records.erase(this->activation_records.begin());

    return act_rec;
}


/**
 * Metoda pro odstraneni vsech aktivacnich zaznamu z kalendare
 */
void CalendarOfEvents::clear () {
    this->activation_records.clear();
}


/**
 * Metoda vraci zda je kalendar prazdny
 * @return  1  Pokud je prazdny
 * @return  0  Pokud neni prazdny
 */
int CalendarOfEvents::empty () {
    return this->activation_records.empty();
}


/**
 * Metoda deaktivuje kalendar udalosti
 * - nastavi "active" na false
 * - odstrani zaznamy z kalendare
 */
void CalendarOfEvents::deactivate () {
    this->active = false;
    this->activation_records.clear();
}
