///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    AttackControl.cpp                                          ///
///   Datum:     únor 2016                                                  ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Bakalářská práce (IBP)                                     ///
///   Projekt:   Generování a ochrana proti DOS útoku na aplikační vrstvě   ///
///   Autor:     Pavel Juhaňák                                              ///
///   Login:     xjuhan01                                                   ///
///   Email:     <xjuhan01@stud.fit.vutbr.cz>                               ///
///   Licence:   Tento zdrojový kód, jakožto i celý tento nástroj           ///
///                je dostupný pod OPEN SOURCE licencí VUT V BRNĚ           ///
///              Úplné znění licence dostupné v souboru `LICENSE.md`        ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////

#include "DoSlib.hpp"

using namespace std;


/**
 * Staticka metoda ovlada provadeni utoku
 * - provadi "next event" algoritmus
 * - dokud neni kalendar udalosti prazdny,
 *   vytahne prvni zaznam z kalendare,
 *   pocka na jeho aktivacni cas a invokuje danou metodu
 * @param  calendar_of_events  Kalendar udalosti, ktery ma prochazet
 * @param  attack_name         Jmeno utoku, ktery metodu invokoval
 */
void AttackControl::run (std::shared_ptr<CalendarOfEvents> calendar_of_events, string attack_name) {
    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> START of AttackControl::run(%s)\n", attack_name.c_str());

    // cykli dokud neni kalendar udalosti prazdny
    while (!calendar_of_events->empty()) {
        // vytahne prvni zaznam z kalendare
        std::shared_ptr<ActivationRecord> act_rec = calendar_of_events->popFront();

        AttackControl::goSleep(act_rec->tv_act_time); // uspi se do casu naplanovane udalosti

        act_rec->event();                             // provede naplanovanou udalost
    }

    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> END of AttackControl::run(%s)\n\n", attack_name.c_str());
}


/**
 * Staticka metoda pro uspani do predaneho casu
 * - pokud je aktuani cas vetsi, nez predany cas, neuspava se
 * @param  tv_act_time  Cas probuzeni
 */
void AttackControl::goSleep (struct timeval tv_act_time) {
    if (tv_act_time.tv_sec == 0 && tv_act_time.tv_usec == 0) // nebude se uspavat
        return;

    struct timeval tv_now;
    struct timeval tv_diff;

    gettimeofday(&tv_now, NULL);                // aktualni cas
    timersub(&tv_act_time, &tv_now, &tv_diff);  // cas do dalsi udalosti

#ifdef LINUX
    struct timespec ts_diff;
    TIMEVAL_TO_TIMESPEC(&tv_diff, &ts_diff);    // prevod na strukturu kterou prijima nanosleep()

    //if (DEBUG_MSG == true) fprintf(stderr, "INFO -> ted je          %s\n", getTime(tv_now).c_str());
    //if (DEBUG_MSG == true) fprintf(stderr, "INFO -> aktivace ma byt %s\n", getTime(tv_act_time).c_str());
    if (DEBUG_MSG == true) fprintf(stderr, "INFO -> going to sleep: nanosleep(%ldms)\n", (ts_diff.tv_sec * 1000) + (ts_diff.tv_nsec / 1000000));

    if (nanosleep(&ts_diff, NULL) != 0) {       // uspani na dobu v ts_diff
        if (errno == EINTR) {                   // spanek prerusen signalem
            if (DEBUG_MSG == true) fprintf(stderr, "INFO -> caught signal in nanosleep(%ldms)\n\n", (ts_diff.tv_sec * 1000) + (ts_diff.tv_nsec / 1000000));
        }
        else if (errno == EINVAL) {             // neuspal se, dostal spatnou hodnotu, udalost se vykona rovnou
            if (DEBUG_MSG == true) fprintf(stderr, "INFO -> EINVAL nanosleep(%ldms)\n\n", (ts_diff.tv_sec * 1000) + (ts_diff.tv_nsec / 1000000));
        }
    }
#else
    if (tv_diff.tv_sec >= 0 && tv_diff.tv_usec >= 0) {
        unsigned long sleep_ms = (tv_diff.tv_sec * 1000) + (tv_diff.tv_usec / 1000);

        if (DEBUG_MSG == true) fprintf(stderr, "INFO -> going to sleep: Sleep(%lums)\n", sleep_ms);

        Sleep(sleep_ms); // uspani na dobu v sleep_ms - spocitana z tv_diff
    }
#endif
}
